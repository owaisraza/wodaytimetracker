package com.dawateislami.periodstrackertool.variables

class Constants {

    companion object{

        const val CODE_START_LAW = "start_law_code"
        const val CODE_END_LAW = "end_law_code"
        const val APP_LOCALE = "locale"
        const val MAX_CALENDAR_COLUMN = 42
        const val IS_ROUTINE_SAVE = "is_save_routine"

    }
}