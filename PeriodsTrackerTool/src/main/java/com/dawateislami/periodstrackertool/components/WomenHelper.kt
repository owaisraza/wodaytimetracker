package com.dawateislami.periodstrackertool.components

import android.content.Context
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.data.database.PeriodsDatabase
import com.dawateislami.periodstrackertool.getIntPreference
import com.dawateislami.periodstrackertool.manager.*
import com.dawateislami.periodstrackertool.variables.Constants
import com.dawateislami.periodstrackertool.variables.PeriodType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.util.*

open class WomenHelper(private val mContext: Context) {

    val data: DataManager = DataManager(PeriodsDatabase.invoke(mContext))
    private val laws: LawsManager = LawsManager(data)
    private val operations: OperationManager = OperationManager(data)
    private val populations: PopulationManager = PopulationManager(data)


    suspend fun startBleeding(periods: Periods) = laws.validateStartBleedingRule(periods)

    suspend fun endBleeding(periods: Periods) = laws.validateEndBleedingRule(periods)

    suspend fun isCurrentMonthRoutineStarted(periods: Periods) = laws.validateCurrentRoutine(periods)

    suspend fun isLastMonthRoutineStarted(periods: Periods) = laws.validateLastRoutine(periods)

    suspend fun addOrRemoveLastRoutineDate(id: Int, periods: Periods, isRemove: Boolean) =
            operations.addOrRemoveLastRoutineDate(id, periods, isRemove)


    suspend fun updatePeriodCurrentRoutineToLastRoutine(){
        val myCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        if(!myCalendar.isNullOrEmpty()){
            val lastDateOfPeriod = myCalendar[myCalendar.size -1].date
            val mCal = normalizeCalendar(Calendar.getInstance())
            val nCal = normalizeCalendar(Calendar.getInstance())
            mCal.time = Date(lastDateOfPeriod)
            mCal.add(Calendar.DAY_OF_MONTH, 15)
            val allowDate = mCal.timeInMillis
            if(allowDate <= nCal.timeInMillis){
                data.updateMyPeriodRoutine(RoutineType.PREVIOUS_ROUTINE.value, RoutineType.LAST_ROUTINE.value)
                data.updateMyPeriodRoutine(RoutineType.LAST_ROUTINE.value, RoutineType.CURRENT_ROUTINE.value)
            }
        }
    }

    suspend fun submitNewRoutine(code: Int, id: Int, periods: Periods) =
            operations.submitNewRoutine(code, id, periods)

    suspend fun submitRestartRoutine(code: Int,id: Int, periods: Periods, isValid: Boolean) =
            operations.submitRestartRoutine(code, id, periods, isValid)

    suspend fun submitRemoveRoutine(periods: Periods) = operations.submitRemoveRoutine(periods)

    suspend fun submitAddRoutine(id: Int, periods: Periods) =
            operations.submitAddRoutine(id, periods)

    suspend fun submitDailySpendRoutine(periods: Periods) =
            operations.submitSpendRoutine(periods)

    suspend fun submitFinishRoutine(id: Int, periods: Periods, endLawCode: Int, time: Long) : Boolean{
        val startLawCode = mContext.getIntPreference(Constants.CODE_START_LAW)
        return operations.submitFinishRoutine(id, periods,startLawCode, endLawCode, time)
    }

    suspend fun submitNonPeriodRoutine(code: Int, id: Int, periods: Periods) =
            operations.submitNonPeriodRoutine(code, id, periods, PeriodType.MENSES.value)

    suspend fun submitPrePeriodBleeding(code: Int, id: Int, periods: Periods) =
            operations.submitNonPeriodRoutine(code, id, periods, PeriodType.MENSES.value)

    suspend fun populatePeriodsCalendar(mList: List<Periods>, month: Int, year: Int) =
            populations.populatePeriodsCalendar(mList, month, year)

    fun getPeriodDaysFromCalendar(month: Int, year: Int, mList: List<Periods>) =
            populations.getPeriodDaysFromCalendar(month, year, mList)

}