package com.dawateislami.periodstrackertool.components

import android.content.Context
import android.util.Log
import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodSchedule
import com.dawateislami.periodstrackertool.data.database.PeriodsDatabase
import com.dawateislami.periodstrackertool.getBooleanPreference
import com.dawateislami.periodstrackertool.manager.DataManager
import com.dawateislami.periodstrackertool.manager.OperationManager
import com.dawateislami.periodstrackertool.manager.normalizeCalendar
import com.dawateislami.periodstrackertool.setPreference
import com.dawateislami.periodstrackertool.variables.Constants
import com.dawateislami.periodstrackertool.variables.PeriodType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.time.Period
import java.util.*
import java.util.Calendar

class Routine(private val mContext: Context) {


    suspend fun saveNormalPeriodRoutine(interval: Int, duration: Int, lastPeriodStartDate: Long, timeOfEndPeriod: Long) : Boolean{
//        if(!mContext.getBooleanPreference(Constants.IS_ROUTINE_SAVE)){
//            mContext.setPreference(Constants.IS_ROUTINE_SAVE, true)
            val mCal = Calendar.getInstance()
            val db = DataManager(PeriodsDatabase.invoke(mContext))
            val data = OperationManager(db)
            val bool = data.updateFuturePeriodSchedule(1, interval, duration, lastPeriodStartDate)
            val diff = calculateNoOfDaysBetweenTwoDates(mCal.time,Date(lastPeriodStartDate)) + 1
            val isCurrentRoutine = diff > 15
            if(isCurrentRoutine && bool){
                mCal.add(Calendar.DAY_OF_MONTH, -15)
                val queryTime = mCal.timeInMillis
                val list = db.getAllPreviousPeriodSchedule(queryTime)
                if(list.size > 1){
                    val size = list.size
                    var total = 1
                    for (periods in list){
                        if(total == size){
                            addRoutineOnMyCalendar(db,total, periods.startPeriodDate, duration, RoutineType.LAST_ROUTINE.value, timeOfEndPeriod)
                        }else{
                            addRoutineOnMyCalendar(db,total, periods.startPeriodDate, duration, RoutineType.PREVIOUS_ROUTINE.value, timeOfEndPeriod)
                        }
                        total++
                    }
                }else addRoutineOnMyCalendar(db,1, lastPeriodStartDate, duration, RoutineType.LAST_ROUTINE.value, timeOfEndPeriod)
            }else {
                addRoutineOnMyCalendar(db,2, lastPeriodStartDate, duration, RoutineType.CURRENT_ROUTINE.value, timeOfEndPeriod)
                addAssumeRoutineAsLast(db, lastPeriodStartDate, duration, interval, timeOfEndPeriod)
            }
            return bool
//        } else return false
    }

    suspend fun saveAbnormalPeriodRoutine(schedule: PeriodSchedule) :Boolean{
//        if(!mContext.getBooleanPreference(Constants.IS_ROUTINE_SAVE)){
//            mContext.setPreference(Constants.IS_ROUTINE_SAVE, true)
            val db = DataManager(PeriodsDatabase.invoke(mContext))
            return OperationManager(db).updateFuturePeriodSchedule(1, schedule.interval, schedule.duration, schedule.startPeriodDate)
 //       } else return false
    }

    private suspend fun addRoutineOnMyCalendar(db: DataManager, routineNo: Int, startDate: Long,duration: Int, routineType: Int, time: Long){
        val mCal = normalizeCalendar(Calendar.getInstance())
        mCal.time = Date(startDate)
        normalizeCalendar(mCal)
        var i : Int = 0
        while(i < duration){
            i++
            val myCalendar = MyCalendar(
                    0,
                    1,
                    routineNo,
                    1,
                    if(duration == 10) 20 else 18,
                    mCal.get(Calendar.DAY_OF_MONTH),
                    mCal.get(Calendar.MONTH),
                    mCal.get(Calendar.YEAR),
                    mCal.timeInMillis,
                    i,
                    1,
                    PeriodType.MENSES.value,
                    routineType,
                    1,
                    time,
                    0,
                    1,
                    Calendar.getInstance().timeInMillis,
                    Calendar.getInstance().timeInMillis
            )
            db.addMyPeriodCalendar(myCalendar)
            mCal.add(Calendar.DAY_OF_MONTH , 1)
        }
    }

    private suspend fun addAssumeRoutineAsLast(db: DataManager,startDate: Long,duration: Int,interval: Int, timeOfEndPeriod: Long): Boolean{
        val mCal = Calendar.getInstance()
        mCal.time = Date(startDate)
        mCal.add(Calendar.DAY_OF_MONTH, - (interval + duration))
        normalizeCalendar(mCal)
        val lastPeriodStartDate = mCal.timeInMillis
        addRoutineOnMyCalendar(db,1, lastPeriodStartDate, duration, RoutineType.LAST_ROUTINE.value, timeOfEndPeriod)
        val startPeriod = mCal.timeInMillis
        val startPeriodDay = mCal.get(Calendar.DAY_OF_MONTH)
        val startPeriodMonth = mCal.get(Calendar.MONTH)
        val startPeriodYear = mCal.get(Calendar.YEAR)
        mCal.add(Calendar.DAY_OF_MONTH, duration - 1)
        val endPeriod = mCal.timeInMillis
        val endPeriodDay = mCal.get(Calendar.DAY_OF_MONTH)
        val endPeriodMonth = mCal.get(Calendar.MONTH)
        val endtPeriodYear = mCal.get(Calendar.YEAR)
        return db.updateMyPeriodSchedule(PeriodSchedule(0, 1, interval, duration, startPeriod, endPeriod,
            startPeriodDay, startPeriodMonth, startPeriodYear, endPeriodDay, endPeriodMonth, endtPeriodYear,
            1, Calendar.getInstance().timeInMillis, Calendar.getInstance().timeInMillis)) > 0
    }

    fun setLocaleInModule(locale: String){
        mContext.setPreference(Constants.APP_LOCALE, locale)
    }
}