package com.dawateislami.periodstrackertool.variables

enum class RoutineType(val value : Int){
    PREVIOUS_ROUTINE(0),
    LAST_ROUTINE(1),
    CURRENT_ROUTINE(2),
    FUTURE_ROUTINE(3),
    INVALID_ROUTINE(4)
}


enum class CalendarType(val value : Int){
    PREVIOUS(1),
    CURRENT(2),
    NEXT(3)
}

enum class PeriodType(val value : Int){
    MENSES(1),
    BLEEDING(2)
}


enum class MessageType(val value : Int){
    CASE_1(1),
    CASE_2(2),
    CASE_3(3),
    CASE_4(4),
    CASE_5(5),
    CASE_6(6),
    CASE_7(7),
    CASE_8(8),
    CASE_9(9),
    CASE_10(10),
    CASE_11(11),
    CASE_12(12),
    CASE_13(13),
    CASE_14(14),
    CASE_15(15),
    CASE_16(16),
    CASE_17(17),
    CASE_18(18),
    CASE_19(19),
    CASE_20(20),
    CASE_21(21),
    CASE_22(22),
    CASE_23(23),
    CASE_24(24),
    CASE_25(25),
    CASE_VALID(0),
    CASE_INVALID(-1)
}
