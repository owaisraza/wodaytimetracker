package com.dawateislami.periodstrackertool.rules

import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodSchedule
import com.dawateislami.periodstrackertool.manager.DataManager
import com.dawateislami.periodstrackertool.variables.MessageType
import java.util.*

class ValidationRules( private val data: DataManager) {


    fun isAlreadyStarted(mList: List<MyCalendar>?) : Boolean{
        return mList.isNullOrEmpty() // New Submit
    }

    fun allowedRestartMonthlyPeriod(mList: List<MyCalendar>?, isPeriodDay: Boolean) : Boolean{
        return !mList.isNullOrEmpty() && !isPeriodDay && mList.size < 10 && mList[0].routineType == 1
    }

    fun askRestartMonthlyPeriod(mList: List<MyCalendar>?, isPeriodDay: Boolean) : Boolean{
        return !mList.isNullOrEmpty() && !isPeriodDay && mList.size == 10 && mList[0].routineType == 1
    }

    fun isRestartMonthlyPeriodAfter(mList: List<MyCalendar>?, date: Long) : Boolean{
        return !mList.isNullOrEmpty() && mList[mList.size -1].date!! < date
    }

    fun isRestartMonthlyPeriodBefore(mList: List<MyCalendar>?, date: Long) : Boolean{
        return !mList.isNullOrEmpty() && mList[0].date!! > date
    }

    fun addPeriodValidation(mList: List<MyCalendar>?) : Boolean{
        return !mList.isNullOrEmpty() && mList[0].routineType == 2 && mList.size <= 10
    }

    fun removePeriodValidation(mList: List<MyCalendar>?) : Boolean{
        return !mList.isNullOrEmpty() && mList[0].routineType == 2 && mList.size >= 3
    }

    fun endPeriodPermission(mList: List<MyCalendar>?) : Boolean{
        return if(!mList.isNullOrEmpty()) mList[0].routineType == 2 else false
    }


}