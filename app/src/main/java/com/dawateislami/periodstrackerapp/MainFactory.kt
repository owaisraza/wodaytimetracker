package com.dawateislami.periodstrackerapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dawateislami.periodstrackertool.components.WomenCalendar
import com.dawateislami.periodstrackertool.manager.LawsManager
import com.dawateislami.periodstrackertool.manager.OperationManager
import com.dawateislami.periodstrackertool.manager.PopulationManager

@Suppress("UNCHECKED_CAST")
class MainFactory(
        private val womenCalendar: WomenCalendar
)  : ViewModelProvider.NewInstanceFactory()  {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(womenCalendar) as T
    }
}