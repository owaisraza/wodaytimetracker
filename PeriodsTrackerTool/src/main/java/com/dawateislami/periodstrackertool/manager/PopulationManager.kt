package com.dawateislami.periodstrackertool.manager

import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodSchedule
import com.dawateislami.periodstrackertool.variables.CalendarType
import com.dawateislami.periodstrackertool.variables.PeriodType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.util.*
import kotlin.collections.ArrayList

class PopulationManager(private val data: DataManager) {


    suspend fun populatePeriodsCalendar(periods: List<Periods>, month: Int, year: Int): List<Periods>{
        val mPeriods: ArrayList<Periods> = ArrayList()
        val nCal = Calendar.getInstance()
        nCal.set(Calendar.MONTH, month)
        nCal.set(Calendar.YEAR, year)
        normalizeCalendar(nCal)

        val startPeriodCalendar = periods[0].gregorianDate.time
        val endPeriodCalendar = periods[periods.size -1].gregorianDate.time
        val isCurrentRoutineAvailable = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val isLastRoutineAvailable = data.getLastRoutineOfPeriod(nCal.timeInMillis, false)
        val bleedingRulesNineValidate = if(!isCurrentRoutineAvailable.isNullOrEmpty()){
            val schedule = data.getMyPeriodSchedule(isCurrentRoutineAvailable[0].date)
            schedule.startPeriodDate < isCurrentRoutineAvailable[isCurrentRoutineAvailable.size -1].date &&
                    isCurrentRoutineAvailable[0].routineStartLaw == 9
        }else false


        val fromDate = if(!isCurrentRoutineAvailable.isNullOrEmpty()){
            val cal = Calendar.getInstance()
            cal.time = Date(isCurrentRoutineAvailable[isCurrentRoutineAvailable.size -1].date)
            cal.add(Calendar.DAY_OF_MONTH, 15)
            cal.timeInMillis
        } else if(!isLastRoutineAvailable.isNullOrEmpty())
            isLastRoutineAvailable[isLastRoutineAvailable.size-1].date
        else startPeriodCalendar
        val monthType = validateMonthAndYear(nCal.timeInMillis)
        val myCalendarList = periodsScheduleToCalendar(
                if(monthType == CalendarType.CURRENT.value || monthType == CalendarType.PREVIOUS.value) data.getMyPeriodAllSchedule(fromDate)
                else if(monthType == CalendarType.NEXT.value) data.getMyScheduleWithDateRange(startPeriodCalendar, endPeriodCalendar)
                else null
        )

        for(p in periods){
            val mCal = Calendar.getInstance()
            val currentDate = p.gregorianDate
            mCal.time = currentDate
            val day = mCal.get(Calendar.DAY_OF_MONTH)
            val months = mCal.get(Calendar.MONTH)
            val years = mCal.get(Calendar.YEAR)
            val mCalendar = data.getMyPeriodCalender(day, months, years)
            val mSchedule = getPeriodsDayFromCalendar(mCal.timeInMillis, myCalendarList)
            val monthTypeId = validateMonthAndYear(mCal.timeInMillis)

            if(mCalendar != null){
                mPeriods.add(Periods(currentDate, mCalendar.routineOrder, true, mCalendar.answer == 1, mCalendar.routineType, mCalendar.periodType,0))
            }else if(mSchedule != null){
                if(monthTypeId == CalendarType.NEXT.value)
                    mPeriods.add(Periods(currentDate, mSchedule.routineOrder, true, mSchedule.answer == 1, mSchedule.routineType, mSchedule.periodType, 0))
                else if(monthTypeId == CalendarType.CURRENT.value){
                     if(isCurrentRoutineAvailable.isNullOrEmpty() || bleedingRulesNineValidate){
                         mPeriods.add(Periods(currentDate, mSchedule.routineOrder, true, mSchedule.answer == 1, mSchedule.routineType, mSchedule.periodType, 0))
                     }else{
                        if(!isCurrentRoutineAvailable.isNullOrEmpty()){
                            val days = calculateNoOfDaysBetweenTwoDates(currentDate, Date(isCurrentRoutineAvailable[isCurrentRoutineAvailable.size -1].date))
                            if(days <= 15) mPeriods.add(Periods(currentDate, 0, false, false, 0, 0, 0))
                            else mPeriods.add(Periods(currentDate, mSchedule.routineOrder, true, mSchedule.answer == 1, mSchedule.routineType, mSchedule.periodType, 0))
                        }else mPeriods.add(Periods(currentDate, mSchedule.routineOrder, true, mSchedule.answer == 1, mSchedule.routineType, mSchedule.periodType, 0))
                     }
                } else mPeriods.add(Periods(currentDate, 0, false, false, 0, 0, 0))
            } else{
                mPeriods.add(Periods(currentDate, 0, false, false, 0, 0, 0))
            }
        }
        return mPeriods
    }

    fun getPeriodDaysFromCalendar(month: Int, year: Int, mlist: List<Periods>) : List<Periods>{
        val routineList : ArrayList<Periods> = ArrayList()
        val cal = Calendar.getInstance()
        for(period in mlist){
            cal.time = period.gregorianDate
            if(month == cal.get(Calendar.MONTH) && year == cal.get(Calendar.YEAR)){
                if(period.isPeriod) routineList.add(period)
            }
        }
        return routineList
    }

    private fun getPeriodsDayFromCalendar(date: Long, mList: List<MyCalendar>): MyCalendar? {
        val nCal = Calendar.getInstance()
        nCal.time = Date(date)
        normalizeCalendar(nCal)
        var mCalendar: MyCalendar? = null
        for (cal in mList){
            val mCal = Calendar.getInstance()
            mCal.time = Date(cal.date)
            if(mCal.timeInMillis == nCal.timeInMillis) mCalendar = cal
        }
        return mCalendar
    }

    private fun periodsScheduleToCalendar(mlist: List<PeriodSchedule>?) : List<MyCalendar>{
        val list :ArrayList<MyCalendar> = ArrayList()
        if(!mlist.isNullOrEmpty()){
            for (schedule in mlist){
                val startPeriod = Date(schedule.startPeriodDate)
                val endPeriod = Date(schedule.endPeriodDate)
                val duration = calculateNoOfDaysBetweenTwoDates(endPeriod, startPeriod) +1
                val mCal = Calendar.getInstance()
                mCal.time =  startPeriod
                normalizeCalendar(mCal)
                var i = 0
                while (i < duration){
                    i++
                    list.add(MyCalendar(0,0,1,1,1,
                            mCal.get(Calendar.DAY_OF_MONTH),
                            mCal.get(Calendar.MONTH),
                            mCal.get(Calendar.YEAR),
                            mCal.timeInMillis,
                            i, 0,
                            0,
                            RoutineType.FUTURE_ROUTINE.value,
                            0,0,0,1,
                            null,null))

                    mCal.add(Calendar.DAY_OF_MONTH , 1)
                }

            }
        }
        return list
    }

}