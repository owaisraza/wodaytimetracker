package com.dawateislami.periodstrackertool.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface PeriodScheduleDao{

    @Query("select * from period_schedule where start_period_date >= :startDate order by start_period_date asc limit 1")
    suspend fun getMyPeriodSchedule(startDate: Long) : PeriodSchedule

    @Query("select * from period_schedule where start_period_date > :startDate order by start_period_date asc limit 1")
    suspend fun getMyNextPeriodSchedule(startDate: Long) : PeriodSchedule

    @Query("select * from period_schedule where start_period_date >= :startDate order by start_period_date asc limit 2")
    suspend fun getMyPeriodSchedules(startDate: Long) : List<PeriodSchedule>

    @Query("select * from period_schedule where start_period_date < :startDate order by start_period_date asc ")
    suspend fun getMyPreviousAllSchedules(startDate: Long) : List<PeriodSchedule>

    @Query("select * from period_schedule where start_period_date >= :startDate and start_period_date <= :endDate ")
    suspend fun getMyScheduleWithDateRange(startDate: Long, endDate: Long) : List<PeriodSchedule>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertMyPeriodSchedule(periodSchedule: PeriodSchedule): Long

    @Query("delete from period_schedule where id = :id ")
    suspend fun deleteMyPeriodSchedule(id: Int)

    @Query("delete from period_schedule where start_period_date >= :time ")
    suspend fun deleteMyFuturePeriodSchedule(time: Long)


}


@Dao
interface MyCalendarDao{


    @Query("select * from my_calendar order by date asc ")
    suspend fun getAllMyPeriodCalendar() : List<MyCalendar>

    @Query("select * from my_calendar where routine_type = 2 and answer = 1 order by routine_order ASC")
    suspend fun getMyPeriodCalendarProgress() : List<MyCalendar>

    @Query("select * from my_calendar where routine_type = :routineType order by date ASC")
    suspend fun getMyPeriodCalendarByDate(routineType: Int) : List<MyCalendar>

    @Query("select * from my_calendar where routine_type = :routineType order by routine_order ASC")
    suspend fun getMyPeriodCalendarByRoutine(routineType: Int) : List<MyCalendar>

    @Query("select * from my_calendar where routine_type = :routineType and period_type = 1 order by routine_order ASC")
    suspend fun getMyActualPeriodCalendar(routineType: Int) : List<MyCalendar>

    @Query("select * from my_calendar where day = :day and month = :month and year = :year order by routine_order ASC")
    suspend fun getMyDailyCalendar(day: Int, month: Int, year: Int) : MyCalendar


    @Query("select * from my_calendar where date >= :date and routine_type = 2")
    suspend fun getMyRemainingCalendarForCase22(date: Long) : List<MyCalendar>

    @Query("select * from my_calendar where date between :startDate and :endDate and routine_type = 2")
    suspend fun getMyRemainingCalendarForCase23(startDate: Long, endDate: Long) : List<MyCalendar>

    @Query("select * from my_calendar group by month order by month desc ")
    suspend fun getPeriodCalendarMonthWise() : List<MyCalendar>

    @Query("select * from my_calendar where routine_no = :routineNo order by date ASC")
    suspend fun getPeriodCalendarAsPerRoutine(routineNo: Int) : List<MyCalendar>

    @Query("select routine_no from my_calendar where routine_no <= :routineNo order by date ASC")
    suspend fun getCalendarPreviousRoutineNo(routineNo: Int) : List<Int>

    @Query("select * from my_calendar group by routine_no order by routine_no DESC")
    suspend fun getPeriodCalendarRoutines() : List<MyCalendar>

    @Query("select * from my_calendar where date between :startDate and :endDate order by routine_order")
    suspend fun getMyCalendarDateWise(startDate: Long, endDate: Long) : List<MyCalendar>

    @Query("select * from my_calendar order by date DESC LIMIT 1")
    suspend fun getMyLastCalendarDate() : MyCalendar

    @Query("select * from my_calendar group by date order by routine_no DESC")
    suspend fun getMyCalendarDuplicationDate(): List<MyCalendar>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addMyPeriodCalendar(myCalendar: MyCalendar): Long

    @Query("update my_calendar set routine_type = :updateRoutine where routine_type = :currentRoutine")
    suspend fun updateMyPeriodRoutineInCalendar(updateRoutine: Int, currentRoutine: Int )

    @Query("update my_calendar set routine_order = :order where day = :day and month = :month and year = :year ")
    suspend fun updateMyPeriodCalendarRoutineOrder(order: Int, day: Int, month: Int, year: Int)

    @Query("update my_calendar set bleeding_status = 1 where routine_type = 2 ")
    suspend fun updateMyPeriodCalendarBleedingStatus()

    @Query("update my_calendar set stop_bleeding_time = :time where routine_type = 2 ")
    suspend fun updateMyPeriodCalendarBleedingStopTime(time: Long)

    @Query("update my_calendar set answer = 1 where routine_type = 2 ")
    suspend fun updateMyPeriodCalendarAsAnswered()

    @Query("update my_calendar set routine_start_law = :code where routine_type = 2 ")
    suspend fun updateMyPeriodCalendarStartLaw(code: Int)

    @Query("update my_calendar set routine_end_law = :endCode where routine_start_law = :startCode and routine_type = 2 ")
    suspend fun updateMyPeriodCalendarEndLaw(startCode: Int, endCode: Int)

    @Query("update my_calendar set routine_type =:routineType and period_type= :periodType where routine_no =:routineNo ")
    suspend fun updateMyPeriodRoutineStatus(routineNo: Int, routineType: Int, periodType: Int,)

    @Query("delete from my_calendar where day =:day and month = :month and year = :year ")
    suspend fun deleteMyPeriodCalendar(day: Int, month: Int, year: Int)

    @Query("delete from my_calendar where date > :endDate ")
    suspend fun deleteMyPeriodCalendarByDate(endDate: Long)

    @Query("delete from my_calendar where month = :month and year = :year ")
    suspend fun deleteMyPeriodCalendar(month: Int, year: Int)

    @Query("delete from my_calendar where routine_type = :routineType")
    suspend fun deleteMyPeriodCalendarByRoutine(routineType: Int)

    @Query("delete from my_calendar")
    suspend fun deleteEntireMyCalendar()
}