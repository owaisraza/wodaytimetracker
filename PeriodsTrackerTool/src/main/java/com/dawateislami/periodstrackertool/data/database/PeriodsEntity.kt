package com.dawateislami.periodstrackertool.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey



@Entity(tableName = "period_schedule")
data class PeriodSchedule(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int,
        @ColumnInfo(name = "profile_id") val profileId: Int,
        @ColumnInfo(name = "interval") val interval: Int,
        @ColumnInfo(name = "duration") val duration: Int,
        @ColumnInfo(name = "start_period_date") val startPeriodDate: Long,
        @ColumnInfo(name = "end_period_date") val endPeriodDate: Long,
        @ColumnInfo(name = "start_day") val startDay: Int,
        @ColumnInfo(name = "start_month") val startMonth: Int,
        @ColumnInfo(name = "start_year") val startYear: Int,
        @ColumnInfo(name = "end_day") val endDay: Int,
        @ColumnInfo(name = "end_month") val endMonth: Int,
        @ColumnInfo(name = "end_year") val endYear: Int,
        @ColumnInfo(name = "is_enable") val isEnable: Int?,
        @ColumnInfo(name = "created_date") val createdDate: Long?,
        @ColumnInfo(name = "modified_date") val modifiedDate: Long?
)

@Entity(tableName = "my_calendar")
data class MyCalendar(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int,
        @ColumnInfo(name = "profile_id") val profileId: Int,
        @ColumnInfo(name = "routine_no") val routineNo: Int,
        @ColumnInfo(name = "routine_start_law") val routineStartLaw: Int,
        @ColumnInfo(name = "routine_end_law") val routineEndLaw: Int,
        @ColumnInfo(name = "day") val day: Int,
        @ColumnInfo(name = "month") val month: Int,
        @ColumnInfo(name = "year") val year: Int,
        @ColumnInfo(name = "date") val date: Long,
        @ColumnInfo(name = "routine_order") val routineOrder: Int,
        @ColumnInfo(name = "answer") val answer: Int,
        @ColumnInfo(name = "period_type") val periodType: Int,
        @ColumnInfo(name = "routine_type") val routineType: Int,
        @ColumnInfo(name = "bleeding_status") val bleedingStatus: Int,
        @ColumnInfo(name = "stop_bleeding_time") val stopBleedingTime: Long,
        @ColumnInfo(name = "note_id") val noteId: Int,
        @ColumnInfo(name = "is_enable") val isEnable: Int,
        @ColumnInfo(name = "created_date") val createdDate: Long?,
        @ColumnInfo(name = "modified_date") val modifiedDate: Long?
)
