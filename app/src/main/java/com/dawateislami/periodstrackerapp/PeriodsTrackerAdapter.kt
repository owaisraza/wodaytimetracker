package com.dawateislami.periodstrackerapp

import android.app.Activity
import android.view.View
import com.dawateislami.periodstrackerapp.databinding.ItemPeriodsTrackerBinding
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.dateEqualOrAfter
import com.dawateislami.periodstrackertool.dateEqualOrBefore
import com.dawateislami.periodstrackertool.variables.PeriodType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.text.SimpleDateFormat
import java.util.*

class PeriodsTrackerAdapter(
        private val mContext: Activity,
        private val callback: CalendarCallback
) : BaseRecyclerViewAdapter<Periods, ItemPeriodsTrackerBinding>()  {

    private var month = 0
    private var year = 0

    override fun getLayout() = R.layout.item_periods_tracker

    override fun onBindViewHolder(holder: Companion.BaseViewHolder<ItemPeriodsTrackerBinding>, position: Int) {

        val periods = items[position]
        val dateCal = Calendar.getInstance()
        val mCal = Calendar.getInstance()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        dateCal.time = items[position].gregorianDate
        val dayValue = dateCal[Calendar.DAY_OF_MONTH]
        val displayMonth = dateCal[Calendar.MONTH]
        val displayYear = dateCal[Calendar.YEAR]
        val currentDay = mCal[Calendar.DAY_OF_MONTH]
        val currentMonth = mCal[Calendar.MONTH]
        val currentYear = mCal[Calendar.YEAR]
        val currentEditDate = dateCal.time
        val formatter = SimpleDateFormat("MMM", Locale.ENGLISH)
        val startEditDate = calendar.time
        calendar.add(Calendar.DAY_OF_MONTH, 40)
        val endEditDate = calendar.time




        if (periods.routineType == RoutineType.CURRENT_ROUTINE.value){
            if(!periods.isAnswered){
                holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_border_curve))
                holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorBlack))
            } else if(periods.periodType == PeriodType.MENSES.value){
                holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_primary_curve))
                holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorWhite))
            }else{
                holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_yellow_curve))
                holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorBlack))
            }
        }else if (periods.routineType == RoutineType.LAST_ROUTINE.value){
            if(periods.periodType == PeriodType.MENSES.value){
                holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_orange_curve))
                holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorWhite))
            }else{
                holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_yellow_curve))
                holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorBlack))
            }
        }else if (periods.routineType == RoutineType.PREVIOUS_ROUTINE.value){
            holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_black_curve))
            holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorWhite))
        }else{
            holder.binding.tvPeriod.setBackground(mContext.getResources().getDrawable(R.drawable.bg_gray_curve))
            holder.binding.tvPeriod.setTextColor(mContext.getResources().getColor(R.color.colorWhite))
        }

        if(items[position].isPeriod) holder.binding.tvPeriod.visibility = View.VISIBLE
        else holder.binding.tvPeriod.visibility = View.INVISIBLE


        holder.binding.tvPeriod.text = "${items[position].periodDay}"
        holder.binding.tvDay.text = "${dayValue}"
        holder.binding.tvMonth.text = formatter.format(dateCal.time)

        holder.binding.root.setOnClickListener {
            callback.onCalendar(items[position])
        }
    }

    fun setMonthAndYearInCalendar(month: Int, year: Int){
        this.month = month
        this.year = year
    }
}