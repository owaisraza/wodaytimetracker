 package com.dawateislami.periodstrackertool.manager

import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.rules.BleedingRules
import com.dawateislami.periodstrackertool.variables.MessageType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.util.*

class LawsManager(
    private val data: DataManager
) : BleedingRules(){


   suspend fun validateStartBleedingRule(periods: Periods): Int{

       val bleedingDate = periods.gregorianDate.time
       val mCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, true)
       val nCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
       val lastStartDate = if(!mCalendar.isNullOrEmpty()) mCalendar[0].date else 0
       val lastEndDate = if(!mCalendar.isNullOrEmpty()) mCalendar[mCalendar.size -1].date else 0
       val mSchedule = data.getMyPeriodSchedule(lastEndDate)
       val expectedStartDate = mSchedule.startPeriodDate
       val intervalBetweenTwoPeriods = calculateNoOfDaysBetweenTwoDates(Date(expectedStartDate), Date(bleedingDate))
       val intervalBetweenLastRoutine = calculateNoOfDaysBetweenTwoDates(Date(bleedingDate), Date(lastEndDate))

       val code = if(!nCalendar.isNullOrEmpty()){
            val actualStartDate = nCalendar[0].date
           if(actualStartDate < bleedingDate) startBleedingRule1(actualStartDate, bleedingDate)
           else MessageType.CASE_INVALID.value
       }else if(nCalendar.isNullOrEmpty() && intervalBetweenLastRoutine < 15){
           MessageType.CASE_INVALID.value
       } else if(intervalBetweenTwoPeriods > 1 && intervalBetweenTwoPeriods < mSchedule.interval){
           startBleedingRule2(bleedingDate, expectedStartDate, mSchedule.duration)
       }else MessageType.CASE_1.value

       return code
   }

    suspend fun validateEndBleedingRule(periods: Periods): Int{

        val actualLastCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, true)
        val mLastCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, false)
        val mNowCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val startBleeding = mNowCalendar[0].date
        val endBleeding = periods.gregorianDate.time

        val endDateLastPeriod = mLastCalendar[mLastCalendar.size - 1].date
        val startDateActualLastPeriod = actualLastCalendar[0].date
        val endDateActualLastPeriod = actualLastCalendar[actualLastCalendar.size - 1].date
        val schedule = data.getMyPeriodSchedule(endDateLastPeriod)

        val lengthOfPeriod = calculateNoOfDaysBetweenTwoDates(Date(endBleeding), Date(startBleeding)) +1
        val routineLastPeriod = actualLastCalendar[actualLastCalendar.size - 1].routineOrder

        val code  = if(lengthOfPeriod == 10){
            MessageType.CASE_20.value
        } else if(lengthOfPeriod > 10){
            endBleedingRule1(startBleeding, endBleeding, schedule.startPeriodDate, schedule.endPeriodDate, startDateActualLastPeriod, endDateActualLastPeriod)
        } else if(lengthOfPeriod < 10) {
            endBleedingRule2(endDateLastPeriod, startBleeding,endBleeding, routineLastPeriod)
        } else MessageType.CASE_INVALID.value
        return code
    }


    suspend fun validateCurrentRoutine(periods: Periods): Boolean{
        val myCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        val mCal = Calendar.getInstance()
        if(!myCalendar.isNullOrEmpty()) mCal.time = Date(myCalendar[0].date)
        mCal.add(Calendar.DAY_OF_MONTH, -10)
        normalizeCalendar(mCal)
        return !myCalendar.isNullOrEmpty() && periods.gregorianDate.time >= mCal.timeInMillis
    }

    suspend fun validateLastRoutine(periods: Periods): Boolean{
        val mCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        val nCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, true)
        val mSchedule = data.getMyNextPeriodSchedule(periods.gregorianDate.time)
        val expectedPeriodStartDate = if(mCalendar.isNullOrEmpty())
            mSchedule.startPeriodDate else mCalendar[0].date

        val mCal = Calendar.getInstance()
        mCal.time = Date(expectedPeriodStartDate)
        mCal.add(Calendar.DAY_OF_MONTH, -14)
        val isCurrentDate = periods.gregorianDate.time < mCal.timeInMillis
        val isValidRoutine = nCalendar[nCalendar.size -1].date <= periods.gregorianDate.time
        return  isCurrentDate && isValidRoutine
    }

    suspend fun isLastRoutineExists() = !data.getMyPeriodCalendarByRoutine(RoutineType.LAST_ROUTINE.value).isNullOrEmpty()

}