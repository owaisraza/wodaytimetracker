package com.dawateislami.periodstrackerapp

import android.content.Context
import android.widget.Toast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun<T: Any> ioThenMain(work : suspend (() -> T?), callback:((T?) -> Unit)) =
    CoroutineScope(Dispatchers.Main).launch {
        val data = CoroutineScope(Dispatchers.IO).async rt@{
            return@rt work()
        }.await()
        callback(data)
    }
