package com.dawateislami.periodstrackertool.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters

class DataWorker(
        context: Context,
        workerParams: WorkerParameters
) : Worker(context, workerParams) {

    override fun doWork(): Result {
        return Result.success()
    }
}