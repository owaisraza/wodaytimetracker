package com.dawateislami.periodstrackerapp

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.dawateislami.periodstrackerapp.databinding.CustomProgressDailogBinding


class ProgressDailogPopup(
    private val activity: Activity
) : Dialog(activity, R.style.Theme_Dialog){

    private lateinit var binding: CustomProgressDailogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(activity),
            R.layout.custom_progress_dailog,
            null,
            false
        )

        setCancelable(false)
        setContentView(binding.root)

    }
}