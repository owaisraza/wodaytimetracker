package com.dawateislami.periodstrackertool.data

import java.util.*

data class Periods(
        var gregorianDate: Date,
        var periodDay: Int,
        var isPeriod: Boolean,
        var isAnswered: Boolean,
        var routineType: Int,
        var periodType: Int,
        var noteId: Int
)

data class MonthlyReport(
        var title: String,
        var month: Int,
        var year: Int,
        var mList: List<Periods>
)

data class Laws(
        var lawsNo: Int,
        var lawsText: String,
        var lawDate: Long
)