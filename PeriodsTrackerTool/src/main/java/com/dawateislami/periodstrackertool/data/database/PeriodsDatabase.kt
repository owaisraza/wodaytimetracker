package com.dawateislami.periodstrackertool.data.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase



@Database(
        entities = [
            PeriodSchedule::class,
            MyCalendar::class
        ],
        version = 1
)
abstract class PeriodsDatabase : RoomDatabase() {

    abstract fun onPeriodScheduleDao(): PeriodScheduleDao
    abstract fun onMyCalendarDao(): MyCalendarDao

    companion object {

        @Volatile
        private var instance: PeriodsDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                PeriodsDatabase::class.java, "PeriodsTracker.db"
            )
                .addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Log.d("PeriodsDatabase", "populating with data...")
                    }
                })
                .addMigrations()
                .build()

    }
}