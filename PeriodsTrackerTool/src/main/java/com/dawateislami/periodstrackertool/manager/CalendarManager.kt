package com.dawateislami.periodstrackertool.manager

import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodSchedule
import com.dawateislami.periodstrackertool.variables.CalendarType
import com.dawateislami.periodstrackertool.variables.Constants.Companion.MAX_CALENDAR_COLUMN
import com.dawateislami.periodstrackertool.variables.PeriodType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.util.*
import kotlin.collections.ArrayList


fun validateMonthAndYear(monthlyDate: Long) : Int{
    val mCal = Calendar.getInstance()
    val cal = Calendar.getInstance()
    mCal.time = Date(monthlyDate)
    mCal.set(Calendar.DAY_OF_MONTH, 1)
    mCal.set(Calendar.HOUR_OF_DAY, 0)
    mCal.set(Calendar.MINUTE, 0)
    mCal.set(Calendar.SECOND, 0)
    mCal.set(Calendar.MILLISECOND, 0)
    cal.set(Calendar.DAY_OF_MONTH, 1)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    return if(mCal.timeInMillis == cal.timeInMillis) CalendarType.CURRENT.value
    else if(mCal.timeInMillis > cal.timeInMillis) CalendarType.NEXT.value
    else CalendarType.PREVIOUS.value
}

fun setUpPeriodsCalendar(month: Int, year: Int) : List<Periods> {
    val list = ArrayList<Periods>()
    var nowCal = Calendar.getInstance()
    nowCal.set(Calendar.MONTH, month)
    nowCal.set(Calendar.YEAR, year)
    nowCal.set(Calendar.DAY_OF_MONTH, 1)
    nowCal = normalizeCalendar(nowCal)
    val firstDayOfTheMonth: Int = nowCal.get(Calendar.DAY_OF_WEEK) - 1
    nowCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth)
    while (list.size < MAX_CALENDAR_COLUMN) {
        list.add(Periods(nowCal.time, 0, false, false, 0, 0,0))
        nowCal.add(Calendar.DAY_OF_MONTH, 1)
    }
    return list
}

fun normalizeCalendar(mCal: Calendar) : Calendar{
    mCal.set(Calendar.HOUR_OF_DAY, 12)
    mCal.set(Calendar.MINUTE, 0)
    mCal.set(Calendar.SECOND, 0)
    mCal.set(Calendar.MILLISECOND, 0)
    return mCal
}

 fun assumeRoutineOnMyCalendar(duration: Int, interval: Int) : List<MyCalendar>{
    val mPeriods: ArrayList<MyCalendar> = ArrayList()
    val mCal = normalizeCalendar(Calendar.getInstance())
    val routine = duration + interval -2
    mCal.add(Calendar.DAY_OF_MONTH, routine)
    normalizeCalendar(mCal)
    var i : Int = 0
    while(i < duration){
        i++
        val myCalendar = MyCalendar(
            0,
            1,
            1,
            1,
            if(duration == 10) 20 else 18,
            mCal.get(Calendar.DAY_OF_MONTH),
            mCal.get(Calendar.MONTH),
            mCal.get(Calendar.YEAR),
            mCal.timeInMillis,
            i,
            1,
            PeriodType.MENSES.value,
            RoutineType.LAST_ROUTINE.value,
            1,
            0,
            0,
            1,
            Calendar.getInstance().timeInMillis,
            Calendar.getInstance().timeInMillis
        )
        mPeriods.add(myCalendar)
        mCal.add(Calendar.DAY_OF_MONTH , 1)
    }
     return mPeriods
}


