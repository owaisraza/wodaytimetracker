package com.dawateislami.periodstrackertool.manager

import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodSchedule
import com.dawateislami.periodstrackertool.data.database.PeriodsDatabase
import com.dawateislami.periodstrackertool.variables.MessageType
import com.dawateislami.periodstrackertool.variables.RoutineType

class DataManager(
        private val db: PeriodsDatabase
) {


    /****************************Schedule***************************************/

    suspend fun getMyPeriodSchedule(startDate: Long) =
            db.onPeriodScheduleDao().getMyPeriodSchedule(startDate)

    suspend fun getMyNextPeriodSchedule(startDate: Long) =
            db.onPeriodScheduleDao().getMyNextPeriodSchedule(startDate)

    suspend fun getMyPeriodAllSchedule(startDate: Long) =
            db.onPeriodScheduleDao().getMyPeriodSchedules(startDate)

    suspend fun getAllPreviousPeriodSchedule(startDate: Long) =
            db.onPeriodScheduleDao().getMyPreviousAllSchedules(startDate)

    suspend fun getMyScheduleWithDateRange(startDate: Long, endDate: Long) =
            db.onPeriodScheduleDao().getMyScheduleWithDateRange(startDate, endDate)


    /****************************Calender***************************************/

    suspend fun getMyPeriodCalendarByDate(routineType: Int) =
        db.onMyCalendarDao().getMyPeriodCalendarByDate(routineType)

    suspend fun getMyPeriodCalendarByRoutine(routineType: Int) =
            db.onMyCalendarDao().getMyPeriodCalendarByRoutine(routineType)

    suspend fun getMyActualPeriodCalendar(routineType: Int) =
        db.onMyCalendarDao().getMyActualPeriodCalendar(routineType)

    suspend fun getMyPeriodCalenderProgress() = db.onMyCalendarDao().getMyPeriodCalendarProgress()

    suspend fun getMyPeriodCalender(day: Int, month: Int, year:Int) =
            db.onMyCalendarDao().getMyDailyCalendar(day, month, year)

    suspend fun getRemainingPeriodCalendar(startDate: Long, endDate: Long, code: Int) : List<MyCalendar> {
        return if(code == MessageType.CASE_22.value) db.onMyCalendarDao().getMyRemainingCalendarForCase22(startDate)
        else if(code == MessageType.CASE_23.value || code == MessageType.CASE_24.value) db.onMyCalendarDao().getMyRemainingCalendarForCase23(startDate, endDate)
        else db.onMyCalendarDao().getMyRemainingCalendarForCase22(startDate)
    }

    suspend fun getPeriodCalendarRoutines() = db.onMyCalendarDao().getPeriodCalendarRoutines()

    suspend fun getAllMyPeriodCalendar() = db.onMyCalendarDao().getAllMyPeriodCalendar()

    suspend fun getAllPeriodCalendarMonth() = db.onMyCalendarDao().getPeriodCalendarMonthWise()

    suspend fun getPeriodCalendarByRoutineNo(routineNo: Int) = db.onMyCalendarDao().getPeriodCalendarAsPerRoutine(routineNo)

    suspend fun getMyLastBleedingDate() = db.onMyCalendarDao().getMyLastCalendarDate()

    suspend fun getMyPeriodsDateWise(startDate: Long, endDate: Long) = db.onMyCalendarDao().getMyCalendarDateWise(startDate, endDate)

    suspend fun updateMyPeriodRoutineStatus(routineNo: Int, routineType: Int, periodType: Int,) =
        db.onMyCalendarDao().updateMyPeriodRoutineStatus(routineNo, routineType, periodType)

    suspend fun getMYCalendarDuplicationDates() = db.onMyCalendarDao().getMyCalendarDuplicationDate()

    /****************************DELETE***************************************/

    suspend fun deleteMyPeriodCalendar(day: Int, month: Int, year: Int) =
            db.onMyCalendarDao().deleteMyPeriodCalendar(day, month, year)

    suspend fun deleteMyPeriodsByDate(date: Long) =
            db.onMyCalendarDao().deleteMyPeriodCalendarByDate(date)

    suspend fun deleteMyCalendarByRoutine(routineType: Int) =
            db.onMyCalendarDao().deleteMyPeriodCalendarByRoutine(routineType)

    suspend fun deleteAllFuturePeriodSchedule(time: Long) =
        db.onPeriodScheduleDao().deleteMyFuturePeriodSchedule(time)

    suspend fun deleteEntireMyCalendar() = db.onMyCalendarDao().deleteEntireMyCalendar()


    /***************************UPDATE***************************************/

    suspend fun addMyPeriodCalendar(myCalendar: MyCalendar) = db.onMyCalendarDao().addMyPeriodCalendar(myCalendar)

    suspend fun updateMyCalendarRoutineOrder(order: Int, day: Int, month: Int, year: Int) =
        db.onMyCalendarDao().updateMyPeriodCalendarRoutineOrder(order, day, month, year)

    suspend fun updateMyCalendarPeriodAnswer() = db.onMyCalendarDao().updateMyPeriodCalendarAsAnswered()

    suspend fun updateMyCalendarPeriodEndLaw(startCode: Int, endCode: Int) = db.onMyCalendarDao().updateMyPeriodCalendarEndLaw(startCode, endCode)

    suspend fun updateMyPeriodRoutine(updateRoutine: Int, currentRoutine: Int) =
            db.onMyCalendarDao().updateMyPeriodRoutineInCalendar(updateRoutine,currentRoutine)

    suspend fun updateMyCalendarPeriodBleedingStatus() = db.onMyCalendarDao().updateMyPeriodCalendarBleedingStatus()

    suspend fun updateMyCalendarPeriodBleedingStopTime(time: Long) =
            db.onMyCalendarDao().updateMyPeriodCalendarBleedingStopTime(time)

    suspend fun updateMyPeriodSchedule(periodSchedule: PeriodSchedule) =
        db.onPeriodScheduleDao().upsertMyPeriodSchedule(periodSchedule)

    /***************************MANUPOLATION***************************************/

    suspend fun getLastRoutineOfPeriod(date: Long, isMensesOnly: Boolean): List<MyCalendar>{
        val lastRoutines = getMyPeriodCalendarByRoutine(RoutineType.LAST_ROUTINE.value)
        val calendars = getMyPeriodCalendarByRoutine(RoutineType.PREVIOUS_ROUTINE.value)
        val schedules = getAllPreviousPeriodSchedule(date)
        val schedule = if(!schedules.isNullOrEmpty()) schedules[schedules.size -1] else null
        val isNotExists = lastRoutines.isNullOrEmpty()
        val mPeriod = if(isNotExists){
           if(calendars.isNullOrEmpty()){
               if(schedule != null) assumeRoutineOnMyCalendar(schedule.duration, schedule.interval)
               else assumeRoutineOnMyCalendar(3, 15)
            }else getPeriodCalendarByRoutineNo(calendars[calendars.size-1].routineNo)
        }else if(isMensesOnly) getMyActualPeriodCalendar(RoutineType.LAST_ROUTINE.value) else lastRoutines

        return mPeriod
    }

}