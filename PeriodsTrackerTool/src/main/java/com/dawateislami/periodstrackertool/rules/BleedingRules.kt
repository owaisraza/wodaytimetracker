package com.dawateislami.periodstrackertool.rules

import android.util.Log
import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.variables.MessageType
import java.util.*

open class BleedingRules {

    fun startBleedingRule1(firstPeriodStartDate: Long, secondStartPeriodDate: Long) : Int{
        val intervalBetweenTwoPeriodsDuration = calculateNoOfDaysBetweenTwoDates(Date(secondStartPeriodDate), Date(firstPeriodStartDate)) + 1
        return if(intervalBetweenTwoPeriodsDuration <= 10) MessageType.CASE_5.value
        else if(intervalBetweenTwoPeriodsDuration in 11..15) MessageType.CASE_6.value
        else MessageType.CASE_INVALID.value
    }

    fun startBleedingRule2(newPeriodStartDate: Long, expectedStartPeriodDate: Long, duration: Int): Int{
        val totalDuration =  calculateNoOfDaysBetweenTwoDates(Date(expectedStartPeriodDate), Date(newPeriodStartDate)) + 1
        val routine = totalDuration + duration

        return if(routine <= 10) MessageType.CASE_8.value
        else if(routine in 11..17) MessageType.CASE_9.value
        else if(routine >= 18) MessageType.CASE_10.value
        else MessageType.CASE_INVALID.value
    }


    fun endBleedingRule1(actualStartDate: Long, actualEndDate: Long, expectedStartDate: Long, expectedEndDate: Long, previousStartDate: Long, previousEndDate: Long): Int{
        val previousRoutine = calculateNoOfDaysBetweenTwoDates(Date(previousEndDate), Date(previousStartDate)) + 1
        
        val commonDaysForCase1Level1 = calculateNoOfDaysBetweenTwoDates(Date(actualEndDate) , Date(expectedStartDate)) + 1
        val commonDaysForCase1Level2 = calculateNoOfDaysBetweenTwoDates(Date(expectedEndDate) , Date(actualStartDate)) + 1

        val commonDaysForCase3Level1 = calculateNoOfDaysBetweenTwoDates(Date(actualStartDate) , Date(expectedStartDate)) + 1
        val commonDaysForCase3Level2 = calculateNoOfDaysBetweenTwoDates(Date(actualEndDate) , Date(expectedStartDate)) + 1

        val code  = if(expectedStartDate > actualStartDate && commonDaysForCase1Level1 < 3) MessageType.CASE_22.value
        else if(expectedStartDate > actualStartDate && commonDaysForCase1Level2 < 3) MessageType.CASE_22.value
        else if(expectedStartDate >= actualStartDate && expectedEndDate <= actualEndDate) MessageType.CASE_23.value
        else if(expectedStartDate < actualStartDate && commonDaysForCase3Level1 >= 3 && commonDaysForCase3Level1 < previousRoutine) MessageType.CASE_24.value
        else if(expectedEndDate > actualEndDate && commonDaysForCase3Level2 >= 3 && commonDaysForCase3Level2 < previousRoutine) MessageType.CASE_24.value
        else MessageType.CASE_INVALID.value
        Log.e("PERIOD", "$code")
        return code
    }

    // as per document scenario define as point 5 'END BLEEDING RULE'
    fun endBleedingRule2(lastPeriodEndDate: Long, currentStartPeriodDate: Long, currentEndPeriodDate: Long, lastDuration: Int) : Int{
        val interval = calculateNoOfDaysBetweenTwoDates(Date(currentStartPeriodDate), Date(lastPeriodEndDate)) + 1
        if(interval >= 15){
            val newRoutine = calculateNoOfDaysBetweenTwoDates(Date(currentEndPeriodDate), Date(currentStartPeriodDate)) + 1
            return if(newRoutine < 3){
                MessageType.CASE_16.value
            }else {
                if(newRoutine in 3 until lastDuration){
                    MessageType.CASE_17.value
                }else if(newRoutine >= 3 && newRoutine >= lastDuration && newRoutine < 10 ){
                    MessageType.CASE_18.value
                }else if(newRoutine >= 10){
                    MessageType.CASE_19.value
                }else{
                    MessageType.CASE_INVALID.value
                }
            }
        }else{
            return MessageType.CASE_INVALID.value
        }
    }
}