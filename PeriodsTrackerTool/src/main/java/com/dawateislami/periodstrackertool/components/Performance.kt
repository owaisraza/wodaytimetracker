package com.dawateislami.periodstrackertool.components

import android.content.Context
import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.Laws
import com.dawateislami.periodstrackertool.data.MonthlyReport
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.getStringPreference
import com.dawateislami.periodstrackertool.manager.DataManager
import com.dawateislami.periodstrackertool.manager.normalizeCalendar
import com.dawateislami.periodstrackertool.manager.setUpPeriodsCalendar
import com.dawateislami.periodstrackertool.variables.Constants
import com.dawateislami.periodstrackertool.variables.PeriodType
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Performance(
    private val data: DataManager
) {

    suspend fun dateWiseReport(start: Long, end: Long) : List<Periods>{
        val list = ArrayList<Periods>()
        val sCal = Calendar.getInstance()
        sCal.time = Date(start)
        val eCal = Calendar.getInstance()
        eCal.time = Date(end)
        var period = 0
        var bleeding = 0
        var normal = 0

        val totalDays = calculateNoOfDaysBetweenTwoDates(Date(end), Date(start)) + 1
        if(totalDays > 0){
            while (list.size <= totalDays) {
                val date = sCal.timeInMillis
                val day = sCal.get(Calendar.DAY_OF_MONTH)
                val month = sCal.get(Calendar.MONTH)
                val year = sCal.get(Calendar.YEAR)
                val mPeriod = data.getMyPeriodCalender(day, month, year)
                if(mPeriod != null) {
                    if(mPeriod.periodType == PeriodType.MENSES.value){
                        period++
                        bleeding = 0
                        normal = 0
                        list.add(Periods(sCal.time, period, true, true, mPeriod.routineType, mPeriod.periodType, mPeriod.noteId))
                    }else{
                        bleeding++
                        period = 0
                        normal = 0
                        list.add(Periods(sCal.time, bleeding, true, true, mPeriod.routineType, mPeriod.periodType, mPeriod.noteId))
                    }
                }
                else {
                    normal++
                    bleeding = 0
                    period = 0
                    list.add(Periods(sCal.time, normal, false, false, 0, 0, 0))
                }
                sCal.add(Calendar.DAY_OF_MONTH, 1)
            }
        }
     return list
    }


    suspend fun monthWiseReport(locale: String): List<MonthlyReport>{
        val mList: ArrayList<MonthlyReport> = ArrayList()
        val months = data.getAllPeriodCalendarMonth()
        var normal = 0
        if(!months.isNullOrEmpty()){
            for (month in months){
                val periods = setUpPeriodsCalendar(month.month, month.year)
                for (i in periods.indices){
                    val mCal = Calendar.getInstance()
                    mCal.time = periods[i].gregorianDate
                    normalizeCalendar(mCal)
                    val day = mCal.get(Calendar.DAY_OF_MONTH)
                    val month = mCal.get(Calendar.MONTH)
                    val year = mCal.get(Calendar.YEAR)
                    val calendar = data.getMyPeriodCalender(day, month, year)
                    if(calendar != null) {
                        periods[i].isAnswered = calendar.answer == 1
                        periods[i].isPeriod = true
                        periods[i].periodDay = calendar.routineOrder
                        periods[i].periodType = calendar.periodType
                        periods[i].routineType = calendar.routineType
                        normal = 0
                    }else{
                        normal++
                        periods[i].isAnswered = false
                        periods[i].isPeriod = false
                        periods[i].periodDay = normal
                        periods[i].periodType = 0
                        periods[i].routineType = 0
                    }
                }
                mList.add(MonthlyReport(makeTitle(month.month, month.year, locale), month.month, month.year, periods))
                normal = 0
            }
        }
        return mList
    }


    private fun makeTitle(month: Int, year: Int, locale: String) : String{
        val mCal = Calendar.getInstance()
        mCal.set(Calendar.MONTH, month)
        mCal.set(Calendar.YEAR, year)
        val format = SimpleDateFormat("MMMM yyyy", Locale(locale))
        return format.format(mCal.timeInMillis)
    }




}