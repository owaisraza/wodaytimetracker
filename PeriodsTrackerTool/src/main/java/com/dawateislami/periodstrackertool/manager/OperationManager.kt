package com.dawateislami.periodstrackertool.manager

import android.util.Log
import com.dawateislami.periodstrackertool.calculateNoOfDaysBetweenTwoDates
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodSchedule
import com.dawateislami.periodstrackertool.variables.MessageType
import com.dawateislami.periodstrackertool.variables.PeriodType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.util.*

class OperationManager(private val data: DataManager) {


    suspend fun submitNewRoutine(code: Int, id: Int, periods: Periods) : Boolean{
        val myCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, true)
        return  if(!myCalendar.isNullOrEmpty()) {
            val bleedingDate = periods.gregorianDate.time
            val duration = calculateNoOfDaysBetweenTwoDates(Date(myCalendar[myCalendar.size -1].date), Date(myCalendar[0].date)) +1
            return startBleedingProcess(code, id, duration, bleedingDate)
        } else false
    }

    suspend fun submitRestartRoutine(code: Int, id: Int, periods: Periods, isValid: Boolean) : Boolean{
        val myLastCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, true)
        val myCurrentCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        return if(!myLastCalendar.isNullOrEmpty() && !myCurrentCalendar.isNullOrEmpty()) {
            return if (isValid){
                val lastDate = Date(myCurrentCalendar[myCurrentCalendar.size-1].date)
                val cal = normalizeCalendar(Calendar.getInstance())
                cal.time = lastDate
                cal.add(Calendar.DAY_OF_MONTH, 1)
                val days = calculateNoOfDaysBetweenTwoDates(periods.gregorianDate, cal.time) +1
                var i = 1
                while (i <= days){
                    addOneDayInMyCalendar(code, id, cal.time)
                    cal.add(Calendar.DAY_OF_MONTH, 1)
                    i++
                }
               true
            } else addOneDayInMyCalendar(code, id, periods.gregorianDate)
        } else false
    }

    suspend fun submitRemoveRoutine(periods: Periods) : Boolean{
        val myCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val bleedingDate = periods.gregorianDate
        val startPeriodDate = myCalendar[0].date
        return if(startPeriodDate <= bleedingDate.time){
            removeBleedingDayProcess(periods)
            true
        }  else false
    }

    suspend fun submitAddRoutine(id: Int, periods: Periods)  : Boolean{
        val myNowCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val order = myNowCalendar[myNowCalendar.size -1].routineOrder + 1
        val bleedingDate = periods.gregorianDate.time
        val endPeriodDate = myNowCalendar[myNowCalendar.size -1].date
//        return if(endPeriodDate < bleedingDate){
//            addBleedingDayProcess(myNowCalendar[myNowCalendar.size -1].routineNo, myNowCalendar[myNowCalendar.size -1].routineStartLaw, id,order, periods, myNowCalendar.size)
//        }  else false
        return addBleedingDayProcess(myNowCalendar[myNowCalendar.size -1].routineNo, myNowCalendar[myNowCalendar.size -1].routineStartLaw, id,order, periods, myNowCalendar.size)
    }

    suspend fun submitFinishRoutine(id: Int, periods: Periods,startCode: Int, endCode: Int, time: Long) : Boolean{
        val myLastCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, true)
        val myNowCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        val bleedingDate = periods.gregorianDate
        val startPeriodDate = if(endCode != MessageType.CASE_16.value) myNowCalendar[0].date else 0
        val lastRoutineEndDate = myLastCalendar[myLastCalendar.size - 1].date
        val duration = calculateNoOfDaysBetweenTwoDates(bleedingDate, Date(startPeriodDate)) + 1
        val interval = calculateNoOfDaysBetweenTwoDates(Date(startPeriodDate), Date(lastRoutineEndDate))

        val isOk=  if (endCode == MessageType.CASE_20.value){
            endBleedingProcess(startCode, endCode, id, interval, duration, startPeriodDate, bleedingDate.time, time)
        } else if (endCode == MessageType.CASE_22.value){
            endBleedingProcessCode22(startCode, endCode,startPeriodDate)
        } else if (endCode == MessageType.CASE_23.value){
            endBleedingProcessCode23(startCode, endCode)
        } else if (endCode == MessageType.CASE_24.value){
            endBleedingProcessCode24(startCode, endCode)
        } else if (endCode == MessageType.CASE_16.value){
            endBleedingProcess16(endCode)
        }else endBleedingProcess(startCode, endCode, id, interval, duration, startPeriodDate, bleedingDate.time, time)

        return isOk
    }

    suspend fun submitSpendRoutine(periods: Periods) : Boolean{
        val mCal = Calendar.getInstance()
        mCal.time = periods.gregorianDate
        val myCalendar = data.getMyPeriodCalender(
                mCal[Calendar.DAY_OF_MONTH], mCal[Calendar.MONTH], mCal[Calendar.YEAR]
        )
        if(myCalendar != null){
            val bleedingDate = periods.gregorianDate.time
            val periodDate = myCalendar.date
            val updated = if(bleedingDate == periodDate) {
                data.addMyPeriodCalendar(MyCalendar(
                        myCalendar.id,
                        myCalendar.profileId,
                        myCalendar.routineNo,
                        myCalendar.routineStartLaw,
                        myCalendar.routineEndLaw,
                        myCalendar.day,
                        myCalendar.month,
                        myCalendar.year,
                        myCalendar.date,
                        myCalendar.routineOrder,
                        1,
                        myCalendar.periodType,
                        myCalendar.routineType,
                        myCalendar.bleedingStatus,
                        0,
                        myCalendar.noteId,
                        myCalendar.isEnable,
                        myCalendar.createdDate,
                        Calendar.getInstance().timeInMillis
                ))
            } else 0
            return updated > 0
        }else{
            return false
        }
    }


    suspend fun submitNonPeriodRoutine(code: Int, id: Int, periods: Periods, periodType: Int): Boolean{
        val cal = Calendar.getInstance()
        cal.time = periods.gregorianDate
        normalizeCalendar(cal)
        val nCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, false)
        val mCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val i = if(!mCalendar.isNullOrEmpty()) mCalendar[mCalendar.size -1].routineOrder + 1 else 1
        val no = if(!mCalendar.isNullOrEmpty()) mCalendar[mCalendar.size -1].routineNo
        else  nCalendar[nCalendar.size -1].routineNo + 1

        val isOk = data.addMyPeriodCalendar(MyCalendar(
                0,
                id,
                no,
                code,
                1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.YEAR),
                cal.timeInMillis,
                i,
                1,
                periodType,
                RoutineType.CURRENT_ROUTINE.value,
                0,
                0,
                0,
                1,
                Calendar.getInstance().timeInMillis,
                Calendar.getInstance().timeInMillis
        ))
        return isOk > 0
    }

    suspend fun addOrRemoveLastRoutineDate(id: Int,periods: Periods, isRemove: Boolean) : Boolean{
        val mCalendar = data.getLastRoutineOfPeriod(periods.gregorianDate.time, false)
        val mCal = Calendar.getInstance()
        mCal.time = periods.gregorianDate
        normalizeCalendar(mCal)
        val myCalendar = MyCalendar(
                0,
                id,
                mCalendar[mCalendar.size-1].routineNo,
                mCalendar[mCalendar.size-1].routineStartLaw,
                mCalendar[mCalendar.size-1].routineEndLaw,
                mCal.get(Calendar.DAY_OF_MONTH),
                mCal.get(Calendar.MONTH),
                mCal.get(Calendar.YEAR),
                mCal.timeInMillis,
                mCalendar[mCalendar.size-1].routineOrder + 1,
                1,
                if(mCalendar.size >= 10) PeriodType.BLEEDING.value else PeriodType.MENSES.value,
                RoutineType.LAST_ROUTINE.value,
                mCalendar[mCalendar.size-1].bleedingStatus,
                mCalendar[mCalendar.size-1].stopBleedingTime,
                0,
                1,
                mCalendar[mCalendar.size-1].createdDate,
                Calendar.getInstance().timeInMillis
        )

        if(!isRemove) data.addMyPeriodCalendar(myCalendar)
        else data.deleteMyPeriodCalendar(mCal.get(Calendar.DAY_OF_MONTH), mCal.get(Calendar.MONTH), mCal.get(Calendar.YEAR))
        return true
    }


    /************************START BLEEDING*********************************/

    private suspend fun startBleedingProcess(code: Int, id: Int,duration: Int, startDate: Long) : Boolean{
        val mCal = Calendar.getInstance()
        val lastBleeding = data.getLastRoutineOfPeriod(startDate, false)
        val bleedings = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val no = if(!bleedings.isNullOrEmpty()) bleedings[bleedings.size-1].routineNo
        else lastBleeding[lastBleeding.size-1].routineNo+1
        if(!bleedings.isNullOrEmpty()){
            val start = bleedings[0].date
            mCal.time = Date(startDate)
            mCal.add(Calendar.DAY_OF_MONTH, duration -1)
            val end = normalizeCalendar(mCal).timeInMillis
            val calculateRoutine = calculateNoOfDaysBetweenTwoDates(Date(end),Date(start)) +1
            if(calculateRoutine <= 10){
                data.deleteMyCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
                addAllDaysInMyCalendar(no, code, id, calculateRoutine , start, 0)
            }else addAllDaysInMyCalendar(no, code, id, duration, startDate, bleedings.size)
        }else {
            addAllDaysInMyCalendar(no, code, id, duration, startDate, bleedings.size)
        }
        return true
    }

    private suspend fun addAllDaysInMyCalendar(no: Int, code: Int, id: Int, duration: Int, startDate: Long, oldBleedingLength: Int){
        var i = 0
        val cal = normalizeCalendar(Calendar.getInstance())
        cal.time = Date(startDate)
        while (i < duration){
            i++
            data.addMyPeriodCalendar(MyCalendar(
                0,
                id,
                no,
                code,
                1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.YEAR),
                cal.timeInMillis,
                i + oldBleedingLength,
                if(i == 1) 1 else 0,
                PeriodType.MENSES.value,
                RoutineType.CURRENT_ROUTINE.value,
                0,
                0,
                0,
                1,
                Calendar.getInstance().timeInMillis,
                Calendar.getInstance().timeInMillis
            ))
            cal.add(Calendar.DAY_OF_MONTH, 1)
        }
    }

    private suspend fun addOneDayInMyCalendar(code:Int, id: Int, date: Date) : Boolean{
        val cal = Calendar.getInstance()
        cal.time = date
        normalizeCalendar(cal)
        val mCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val i = mCalendar[mCalendar.size -1].routineOrder + 1
        val size = mCalendar.size

        val isOk = data.addMyPeriodCalendar(MyCalendar(
                0,
                id,
                mCalendar[mCalendar.size-1].routineNo,
                code,
                1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.YEAR),
                cal.timeInMillis,
                i,
                1,
                if(size <= 10) PeriodType.MENSES.value else PeriodType.BLEEDING.value,
                RoutineType.CURRENT_ROUTINE.value,
                0,
                0,
                0,
                1,
                Calendar.getInstance().timeInMillis,
                Calendar.getInstance().timeInMillis
        ))
        updateCurrentPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        return isOk > 0
    }

    private suspend fun updateLastMonthPeriodsCalendar(){
        val myCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        for(c in myCalendar){
            val mCal = MyCalendar(c.id, c.profileId,c.routineNo, c.routineStartLaw, c.routineEndLaw,c.day,c.month,c.year,c.date,c.routineOrder,c.answer,c.periodType,RoutineType.LAST_ROUTINE.value,c.bleedingStatus,c.stopBleedingTime,c.noteId,c.isEnable,c.createdDate,Calendar.getInstance().timeInMillis)
            data.addMyPeriodCalendar(mCal)
        }
    }


    /************************END BLEEDING*********************************/

    private suspend fun endBleedingProcess(startCode: Int, endCode: Int, id: Int, interval: Int, duration: Int, startDate: Long, endDate: Long, time: Long) : Boolean{
        data.deleteMyPeriodsByDate(endDate)
        data.updateMyCalendarPeriodAnswer()
        data.updateMyCalendarPeriodBleedingStatus()
        data.updateMyCalendarPeriodBleedingStopTime(time)
        data.updateMyCalendarPeriodEndLaw(startCode, endCode)
        data.deleteAllFuturePeriodSchedule(startDate)
        return updateFuturePeriodSchedule(id, interval -1, duration, startDate)
    }

    private suspend fun endBleedingProcess16(code: Int): Boolean{
        val myCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        for(p in myCalendar){
            data.addMyPeriodCalendar(MyCalendar(p.id, p.profileId,p.routineNo, p.routineStartLaw, code, p.day, p.month, p.year, p.date, p.routineOrder, p.answer, PeriodType.BLEEDING.value, p.routineType, 1, p.stopBleedingTime,p.noteId, p.isEnable, p.createdDate, Calendar.getInstance().timeInMillis))
        }
        return true
    }

    private suspend fun endBleedingProcessCode22(startCode: Int, endCode: Int, startPeriodDate: Long): Boolean{
        val myLastCalendar = data.getLastRoutineOfPeriod(startPeriodDate, true)
        val cal = normalizeCalendar(Calendar.getInstance())
        cal.time = Date(startPeriodDate)
        val start = cal.timeInMillis
        val duration = myLastCalendar.size
        cal.add(Calendar.DAY_OF_MONTH, duration)
        val end = cal.timeInMillis
        val interval = calculateNoOfDaysBetweenTwoDates(Date(start), Date(myLastCalendar[myLastCalendar.size - 1].date))
        data.updateMyCalendarPeriodBleedingStatus()
        data.updateMyCalendarPeriodAnswer()
        val nonPeriods = data.getRemainingPeriodCalendar(end, start, 22)
        data.deleteAllFuturePeriodSchedule(start)
        for(p in nonPeriods){
            data.addMyPeriodCalendar(MyCalendar(p.id,  p.profileId,p.routineNo, p.routineStartLaw, endCode, p.day, p.month, p.year, p.date, p.routineOrder, p.answer, PeriodType.BLEEDING.value, p.routineType, 1,p.stopBleedingTime, p.noteId, p.isEnable, p.createdDate, Calendar.getInstance().timeInMillis))
        }
        data.updateMyCalendarPeriodEndLaw(startCode, endCode)
        return updateFuturePeriodSchedule(1, interval -1, duration, start)
    }

    private suspend fun endBleedingProcessCode23(startCode: Int, endCode: Int): Boolean{
        val myLastCalendar = data.getLastRoutineOfPeriod(Calendar.getInstance().timeInMillis, true)
        val myNowCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val schedule = data.getMyPeriodSchedule(myNowCalendar[0].date)
        val cal = normalizeCalendar(Calendar.getInstance())
        return if(schedule != null){
            cal.time = Date(schedule.startPeriodDate)
            val start = cal.timeInMillis
            val duration = schedule.duration
            cal.add(Calendar.DAY_OF_MONTH, duration - 1)
            val end = cal.timeInMillis
            val interval = calculateNoOfDaysBetweenTwoDates(Date(start), Date(myLastCalendar[myLastCalendar.size - 1].date))
            val actualBleedings = data.getRemainingPeriodCalendar(start, end, 23)
            data.updateMyCalendarPeriodBleedingStatus()
            data.updateMyCalendarPeriodAnswer()
            val periods = getNonPeriodsBleedingDays(myNowCalendar,actualBleedings)
            data.deleteAllFuturePeriodSchedule(start)
            for(p in periods){
                data.addMyPeriodCalendar(MyCalendar(p.id,  p.profileId,p.routineNo, p.routineStartLaw, endCode, p.day, p.month, p.year, p.date, p.routineOrder, p.answer, PeriodType.BLEEDING.value, p.routineType, 1, p.stopBleedingTime,p.noteId, p.isEnable, p.createdDate, Calendar.getInstance().timeInMillis))
            }
            data.updateMyCalendarPeriodEndLaw(startCode, endCode)
            updateFuturePeriodSchedule(1, interval -1, duration, start)
        } else false
    }

    private suspend fun endBleedingProcessCode24(startCode: Int, endCode: Int): Boolean{
        val myLastCalendar = data.getLastRoutineOfPeriod(Calendar.getInstance().timeInMillis, true)
        val myNowCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val schedule = data.getMyPeriodSchedule(myLastCalendar[myLastCalendar.size -1].date)
        val cal = normalizeCalendar(Calendar.getInstance())
        var interval = 0
        var duration = 0
        return if(schedule != null){
            val expectedStartDate = schedule.startPeriodDate
            val expectedEndDate = schedule.endPeriodDate
            val actualStartDate = myNowCalendar[0].date
            val actualEndDate = myNowCalendar[myNowCalendar.size - 1].date
            val lastPeriodEndDate = myLastCalendar[myLastCalendar.size -1].date
            interval = calculateNoOfDaysBetweenTwoDates(Date(actualStartDate), Date(lastPeriodEndDate))
            val actualBleedings = if(expectedStartDate < actualStartDate){
                Log.e("PERIOD", "expectedStartDate < actualStartDate")
                duration = calculateNoOfDaysBetweenTwoDates(Date(expectedEndDate) , Date(actualStartDate)) + 1
                cal.time = Date(actualStartDate)
                val periodStartDate = cal.timeInMillis
                cal.add(Calendar.DAY_OF_MONTH, duration -1)
                val periodEndDate = cal.timeInMillis
                data.getRemainingPeriodCalendar(periodStartDate, periodEndDate, 24)
            }else{
                Log.e("PERIOD", "expectedStartDate > actualStartDate")
                duration = calculateNoOfDaysBetweenTwoDates(Date(actualEndDate) , Date(expectedStartDate)) + 1
                cal.time = Date(actualEndDate)
                val periodEndDate = cal.timeInMillis
                cal.add(Calendar.DAY_OF_MONTH, -duration +1)
                val periodStartDate = cal.timeInMillis
                data.getRemainingPeriodCalendar(periodStartDate, periodEndDate, 24)
            }
            if(interval > 0 && duration > 0){
                data.updateMyCalendarPeriodBleedingStatus()
                data.updateMyCalendarPeriodAnswer()
                val periods = getNonPeriodsBleedingDays(myNowCalendar,actualBleedings)
                val bleedingStartDate = actualBleedings[0].date
                data.deleteAllFuturePeriodSchedule(bleedingStartDate)
                for(p in periods){
                    data.addMyPeriodCalendar(MyCalendar(p.id,  p.profileId,p.routineNo, p.routineStartLaw, endCode, p.day, p.month, p.year, p.date, p.routineOrder, 1, PeriodType.BLEEDING.value, p.routineType, 1,p.stopBleedingTime, p.noteId, p.isEnable, p.createdDate, Calendar.getInstance().timeInMillis))
                }
                data.updateMyCalendarPeriodEndLaw(startCode, endCode)
                updateFuturePeriodSchedule(1, interval -1, duration, bleedingStartDate)
            }else{
                Log.e("PERIOD", "$interval > 0 && $duration > 0")
                false
            }
        } else{
            Log.e("PERIOD", "schedule != null")
            false
        }
    }



     suspend fun updateFuturePeriodSchedule(id: Int,interval: Int, duration: Int, startDate: Long) : Boolean {
        var isPeriodsSchedule = false
        var cal = Calendar.getInstance()
        var yCal = Calendar.getInstance()
        yCal.add(Calendar.YEAR, 6)
        while (cal.get(Calendar.YEAR) < yCal[Calendar.YEAR]){

            if(!isPeriodsSchedule) {
                cal.time = Date(startDate)
                cal = normalizeCalendar(cal)
            } else cal.add(Calendar.DAY_OF_MONTH, 1)

            val startPeriod = cal.timeInMillis
            val startPeriodDay = cal.get(Calendar.DAY_OF_MONTH)
            val startPeriodMonth = cal.get(Calendar.MONTH)
            val startPeriodYear = cal.get(Calendar.YEAR)
            val routine = duration -1
            cal.add(Calendar.DAY_OF_MONTH, routine)
            val endPeriod = cal.timeInMillis
            val endPeriodDay = cal.get(Calendar.DAY_OF_MONTH)
            val endPeriodMonth = cal.get(Calendar.MONTH)
            val endtPeriodYear = cal.get(Calendar.YEAR)

            val periods = PeriodSchedule(0, id, interval, duration, startPeriod, endPeriod, startPeriodDay,
                startPeriodMonth, startPeriodYear, endPeriodDay, endPeriodMonth, endtPeriodYear, 1,
                Calendar.getInstance().timeInMillis, Calendar.getInstance().timeInMillis)

            val isPeriod = data.updateMyPeriodSchedule(periods) > 0
            if(isPeriod){
                isPeriodsSchedule = true
                cal.add(Calendar.DAY_OF_MONTH, interval)
                Log.e("ROUTINE_OPERATION", "interval $interval and duration: $duration")
            }
        }
        return isPeriodsSchedule
    }

    /************************ADD BLEEDING*********************************/

    private suspend fun addBleedingDayProcess(no: Int, code: Int, id: Int, order: Int, periods: Periods, size: Int): Boolean{
        val cal = normalizeCalendar(Calendar.getInstance())
        cal.time = periods.gregorianDate
        val isOk = data.addMyPeriodCalendar(MyCalendar(
                0,
                id,
                no,
                code,
                1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.YEAR),
                cal.timeInMillis,
                order,
                1,
                if(size <= 10) PeriodType.MENSES.value else PeriodType.BLEEDING.value,
                RoutineType.CURRENT_ROUTINE.value,
                0,
                0,
                0,
                1,
                Calendar.getInstance().timeInMillis,
                Calendar.getInstance().timeInMillis
        )) > 0
        updateCurrentPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        return isOk
    }

    private suspend fun updateCurrentPeriodCalendar(routineType: Int){
        val mList = if(routineType == RoutineType.CURRENT_ROUTINE.value)
            data.getMyPeriodCalendarByDate(RoutineType.CURRENT_ROUTINE.value)
        else data.getMyPeriodCalendarByDate(RoutineType.LAST_ROUTINE.value)
        var i = 0
        mList.forEach { p ->
            i++
            val calendar =  if(i > 10){
                MyCalendar(p.id, p.profileId, p.routineNo, p.routineStartLaw, p.routineEndLaw, p.day, p.month, p.year, p.date, i, p.answer,
                        PeriodType.BLEEDING.value, routineType, p.bleedingStatus, p.stopBleedingTime, p.noteId, p.isEnable ,p.createdDate ,Calendar.getInstance().timeInMillis)

            }else{
                MyCalendar(p.id, p.profileId, p.routineNo, p.routineStartLaw, p.routineEndLaw, p.day, p.month, p.year, p.date, i, p.answer,
                        PeriodType.MENSES.value, routineType, p.bleedingStatus, p.stopBleedingTime, p.noteId, p.isEnable ,p.createdDate ,Calendar.getInstance().timeInMillis)
            }
            data.addMyPeriodCalendar(calendar)
        }

    }

    /************************REMOVE BLEEDING*********************************/

    private suspend fun removeBleedingDayProcess(periods: Periods) {
        val cal = Calendar.getInstance()
        cal.time = periods.gregorianDate
        normalizeCalendar(cal)
        val remaining = data.getRemainingPeriodCalendar(cal.timeInMillis, 0, 0)
        var order = remaining[0].routineOrder
        val startPeriodDay = cal.get(Calendar.DAY_OF_MONTH)
        val startPeriodMonth = cal.get(Calendar.MONTH)
        val startPeriodYear = cal.get(Calendar.YEAR)
        data.deleteMyPeriodCalendar(startPeriodDay,startPeriodMonth,startPeriodYear)
        for (i in 1 until remaining.size){
            val periods = remaining[i]
            data.updateMyCalendarRoutineOrder(order, periods.day, periods.month, periods.year)
            order++
        }
    }

    /***************************************************************/

    private fun getNonPeriodsBleedingDays(allBleedings: List<MyCalendar>, actualBleedings: List<MyCalendar>): List<MyCalendar> {
        return allBleedings.filterNot { prefStudent ->
            actualBleedings.any {
                prefStudent.date == it.date
            }
        }
    }
}