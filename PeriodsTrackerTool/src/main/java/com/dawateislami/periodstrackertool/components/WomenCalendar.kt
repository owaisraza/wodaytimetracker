package com.dawateislami.periodstrackertool.components

import android.content.Context
import com.dawateislami.periodstrackertool.*
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.manager.normalizeCalendar
import com.dawateislami.periodstrackertool.variables.Constants
import com.dawateislami.periodstrackertool.variables.MessageType
import com.dawateislami.periodstrackertool.variables.RoutineType
import java.util.*

class WomenCalendar(
    private val mContext: Context,
) : WomenHelper(mContext) {


    suspend fun validateCurrentStatusOfBleeding(): Int{
        val mCal = normalizeCalendar(Calendar.getInstance())
        val previousCalendar = data.getLastRoutineOfPeriod(mCal.timeInMillis, true)
        val currentCalendar = data.getMyActualPeriodCalendar(RoutineType.CURRENT_ROUTINE.value)
        val mSchedule = if(!previousCalendar.isNullOrEmpty()) data.getMyPeriodSchedule(previousCalendar[previousCalendar.size -1].date) else null
        val cal = normalizeCalendar(Calendar.getInstance())
        if(mSchedule != null){
            cal.time = if(currentCalendar.isNullOrEmpty()) Date(mSchedule.startPeriodDate) else Date(currentCalendar[0].date)
            cal.add(Calendar.DAY_OF_MONTH, 10)
            val code = if(mCal.timeInMillis >= mSchedule.startPeriodDate && currentCalendar.isNullOrEmpty()){
                MessageType.CASE_1.value
            }else if(mCal.timeInMillis >= mSchedule.endPeriodDate && !currentCalendar.isNullOrEmpty() && currentCalendar[0].bleedingStatus == 0){
                MessageType.CASE_13.value
            }else if(mCal.timeInMillis == cal.timeInMillis && !currentCalendar.isNullOrEmpty() && currentCalendar[0].bleedingStatus == 0){
                MessageType.CASE_20.value
            }else{
                MessageType.CASE_INVALID.value
            }
            return code
        } else return MessageType.CASE_INVALID.value
    }

    suspend fun bleedingProcessForPeriod(id: Int, periods: Periods,code: Int): Boolean{
       mContext.setPreference(Constants.CODE_START_LAW, code)
       val isOk = if(code == MessageType.CASE_1.value){
            submitNewRoutine(code, id, periods)
        } else if(code == MessageType.CASE_5.value){
            submitRestartRoutine(code, id, periods, true)
        } else if(code == MessageType.CASE_6.value){
            submitRestartRoutine(code, id, periods, false)
        } else if(code == MessageType.CASE_8.value){
           submitNewRoutine(code, id, periods)
        } else if(code == MessageType.CASE_9.value){
           submitNonPeriodRoutine(code, id, periods)
        } else if(code == MessageType.CASE_10.value){
           submitPrePeriodBleeding(code, id, periods)
        } else  false

        return isOk
    }

    fun getMessageForBleedingRule(rule: Int) : String{
        return when(rule){
            MessageType.CASE_1.value ->  mContext.applyResource().getString(R.string.case_1)
            MessageType.CASE_2.value ->  mContext.applyResource().getString(R.string.case_2)
            MessageType.CASE_3.value ->  mContext.applyResource().getString(R.string.case_3)
            MessageType.CASE_4.value ->  mContext.applyResource().getString(R.string.case_4)
            MessageType.CASE_5.value -> mContext.applyResource().getString(R.string.case_5)
            MessageType.CASE_6.value -> mContext.applyResource().getString(R.string.case_6)
            MessageType.CASE_7.value -> mContext.applyResource().getString(R.string.case_7)
            MessageType.CASE_8.value -> mContext.applyResource().getString(R.string.case_8)
            MessageType.CASE_9.value -> mContext.applyResource().getString(R.string.case_9)
            MessageType.CASE_10.value -> mContext.applyResource().getString(R.string.case_10)
            MessageType.CASE_11.value -> mContext.applyResource().getString(R.string.case_11)
            MessageType.CASE_12.value -> mContext.applyResource().getString(R.string.case_12)
            MessageType.CASE_13.value -> mContext.applyResource().getString(R.string.case_13)
            MessageType.CASE_14.value -> mContext.applyResource().getString(R.string.case_14)
            MessageType.CASE_15.value -> mContext.applyResource().getString(R.string.case_15)
            MessageType.CASE_16.value -> mContext.applyResource().getString(R.string.case_16)
            MessageType.CASE_17.value -> mContext.applyResource().getString(R.string.case_17)
            MessageType.CASE_18.value -> mContext.applyResource().getString(R.string.case_18)
            MessageType.CASE_19.value -> mContext.applyResource().getString(R.string.case_19)
            MessageType.CASE_20.value -> mContext.applyResource().getString(R.string.case_20)
            MessageType.CASE_21.value -> ""
            MessageType.CASE_22.value -> mContext.applyResource().getString(R.string.case_22)
            MessageType.CASE_23.value -> mContext.applyResource().getString(R.string.case_23)
            MessageType.CASE_24.value -> mContext.applyResource().getString(R.string.case_24)
            else -> mContext.applyResource().getString(R.string.case_0)
        }
    }

    suspend fun lawMakingText(rule: String, code: Int): String{
        val nowCalendar = data.getMyPeriodCalendarByRoutine(RoutineType.CURRENT_ROUTINE.value)
        val lastCalendar = data.getLastRoutineOfPeriod(0, false)
        val param = if(!nowCalendar.isNullOrEmpty() && !lastCalendar.isNullOrEmpty() ){
            val mSchedule = data.getMyPeriodSchedule(nowCalendar[0].date)
            if(code == MessageType.CASE_13.value) "${lastCalendar[lastCalendar.size - 1].routineOrder}"
            else if(code == MessageType.CASE_17.value && mSchedule != null) "${dateTimeInString(mSchedule.startPeriodDate, "dd-MMM-yyyy")}"
            else ""
        } else ""
        return rule.replace("@", param)
    }

    suspend fun getProgressOfPeriod(date: Long): Int{
        val currentCalendar = data.getMyPeriodCalenderProgress()
        val schedule = data.getMyPeriodSchedule(date)
        return if(currentCalendar.isNullOrEmpty())  calculateNoOfDaysBetweenTwoDates(Date(schedule.startPeriodDate), Date(date)) + 1
        else currentCalendar.size
    }

    suspend fun saveLastPeriodRoutine(interval: Int, duration: Int, date: Long, time: Long){
        Routine(mContext).saveNormalPeriodRoutine(interval, duration, date,time)
    }

    fun getStartLawCode() = mContext.getIntPreference(Constants.CODE_START_LAW)
    fun unsetStartLawCode() = mContext.removePreference(Constants.CODE_START_LAW)
}