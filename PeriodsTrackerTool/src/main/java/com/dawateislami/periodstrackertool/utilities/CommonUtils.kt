package com.dawateislami.periodstrackertool

import android.content.Context
import android.content.res.Resources
import com.dawateislami.periodstrackertool.data.database.MyCalendar
import com.dawateislami.periodstrackertool.utilities.ResourceUtils
import com.dawateislami.periodstrackertool.variables.Constants
import com.dawateislami.periodstrackertool.variables.MessageType
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun Context.applyResource(): Resources {
    val locale = getStringPreference(Constants.APP_LOCALE)
    val context: Context = ResourceUtils.setLocale(this, locale)!!
    return context.resources
}

fun Context.nativeLocale() = Locale(getStringPreference(Constants.APP_LOCALE)!!)

fun calculateNoOfDaysBetweenTwoDates(date1: Date, date2: Date): Int {
    val diff: Long = date1.getTime() - date2.getTime()
    val seconds = diff / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    val days = hours / 24
    return days.toInt()
}

fun dateEqualOrAfter(date1: Date, date2: Date): Boolean {
    return if (date1.equals(date2)) true else date1.after(date2)
}

fun dateEqualOrBefore(date1: Date, date2: Date): Boolean {
    return if (date1.equals(date2)) true else date1.before(date2)
}


fun dateAfter(date1: Date, date2: Date): Boolean {
    return  date1.after(date2)
}

fun dateBefore(date1: Date, date2: Date): Boolean {
    return  date1.before(date2)
}

fun datesEqualTo(date1: Long, date2: Long) : Boolean{
    val cal1 = Calendar.getInstance()
    cal1.time = Date(date1)
    val cal2 = Calendar.getInstance()
    cal2.time = Date(date2)
    return  cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH) &&
            cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
}



fun isIntervalChanged(lastInterval: Int, currentInterval: Int): Int{ return 0 }

fun dateTimeInString(milliseconds: Long, format: String):String?{
    val sdfDate = SimpleDateFormat(format, Locale.ENGLISH)
    val now = Date(milliseconds)
    return sdfDate.format(now)
}

fun dateTimeInLong(time: String?, format: String): Long {
    val sdf = SimpleDateFormat(format, Locale.ENGLISH)
    var date: Date? = null
    try {
        date = sdf.parse(time)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return date!!.time
}


