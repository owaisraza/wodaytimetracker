package com.dawateislami.periodstrackerapp

import androidx.lifecycle.ViewModel
import com.dawateislami.periodstrackertool.components.WomenCalendar
import com.dawateislami.periodstrackertool.data.Periods
import java.util.*

class MainViewModel(
    private val womenCalendar: WomenCalendar
) : ViewModel() {

    suspend fun isCurrentMonthRoutineStarted(periods: Periods) = womenCalendar.isCurrentMonthRoutineStarted(periods)



    suspend fun spendDailyRoutine(periods: Periods) = womenCalendar.submitDailySpendRoutine(periods)

    fun allowedNewPeriodsRoutine(){
        ioThenMain({womenCalendar.updatePeriodCurrentRoutineToLastRoutine()},{})
    }

    suspend fun startRoutine(periods: Periods) = womenCalendar.startBleeding(periods)

    suspend fun endRoutine(periods: Periods) = womenCalendar.endBleeding(periods)


    /*********************************************************************/
    suspend fun populatePeriodsCalendar(mList: List<Periods>, month: Int, year: Int) =
            womenCalendar.populatePeriodsCalendar(mList, month, year)


    fun getPeriodDaysFromCalendar(month: Int, year: Int, mList: List<Periods>) =
            womenCalendar.getPeriodDaysFromCalendar(month, year, mList)

    suspend fun bleedingProcessForPeriod(id: Int, periods: Periods, code: Int) =
            womenCalendar.bleedingProcessForPeriod(id, periods, code)

    suspend fun submitRemoveRoutine(periods: Periods) =
            womenCalendar.submitRemoveRoutine(periods)

    suspend fun submitAddRoutine(id: Int, periods: Periods) =
            womenCalendar.submitAddRoutine(id, periods)

    suspend fun submitFinishRoutine(id: Int, periods: Periods, code: Int, time: Long) =
            womenCalendar.submitFinishRoutine(id, periods, code, time)

    fun getMessageForBleedingRule(code: Int)  = womenCalendar.getMessageForBleedingRule(code)

    suspend fun getLawMakingRule(rule: String, code: Int) = womenCalendar.lawMakingText(rule, code)

    suspend fun validationOfCurrentStatus() = womenCalendar.validateCurrentStatusOfBleeding()

    suspend fun progressOfPeriod(date: Long) = womenCalendar.getProgressOfPeriod(date)


    fun savePeriodsRoutine(){
        val calendar = Calendar.getInstance()
        ioThenMain({ womenCalendar.saveLastPeriodRoutine(28, 5, calendar.timeInMillis,0) },{

        })
    }
}