package com.dawateislami.periodstrackertool.utilities

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import java.util.*


class ResourceUtils {

    companion object{

        fun setLocale(context: Context, language: String?): Context? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                updateResources(context, language)
            } else updateResourcesLegacy(context, language)
        }

        @TargetApi(Build.VERSION_CODES.N)
        private fun updateResources(context: Context, language: String?): Context? {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val configuration: Configuration = context.getResources().getConfiguration()
            configuration.setLocale(locale)
            return context.createConfigurationContext(configuration)
        }

        private fun updateResourcesLegacy(context: Context, language: String?): Context? {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val resources: Resources = context.getResources()
            val configuration: Configuration = resources.getConfiguration()
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.getDisplayMetrics())
            return context
        }
    }
}