package com.dawateislami.periodstrackerapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.dawateislami.periodstrackerapp.databinding.ActivityMainBinding
import com.dawateislami.periodstrackertool.data.Periods
import com.dawateislami.periodstrackertool.manager.*
import com.dawateislami.periodstrackertool.variables.MessageType
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() , KodeinAware , CalendarCallback {

    override val kodein by kodein()
    private val mainViewModel : MainViewModel by instance()

    private lateinit var binding: ActivityMainBinding
    private val mCal = Calendar.getInstance()
    private lateinit var formatter: SimpleDateFormat
    private lateinit var progressDialog: ProgressDailogPopup
    private val adapter by lazy{
        PeriodsTrackerAdapter(this, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        formatter = SimpleDateFormat("MMMM yyyy", Locale.ENGLISH)
        mainViewModel.savePeriodsRoutine()
        binding.calendar.rcyCalender.adapter = adapter
        progressDialog = ProgressDailogPopup(this@MainActivity)
        binding.bar.tvPeriodMonth.text = formatter.format(mCal.time)


        binding.bar.imgLeft.setOnClickListener {
            mCal.add(Calendar.MONTH, -1)
            binding.bar.tvPeriodMonth.text =  formatter.format(mCal.time)
            populateCalendar(mCal)
        }

        binding.bar.imgRight.setOnClickListener {
            if(mCal.get(Calendar.YEAR) < 2051){
                mCal.add(Calendar.MONTH, 1)
                binding.bar.tvPeriodMonth.text =  formatter.format(mCal.time)
                populateCalendar(mCal)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        populateCalendar(mCal)
        validationOfCurrentBleedingStatus()
        mainViewModel.allowedNewPeriodsRoutine()
        ioThenMain({ mainViewModel.progressOfPeriod(Calendar.getInstance().timeInMillis) },{
            toast("progress: $it")
        })
    }

    override fun onCalendar(periods: Periods) {
        showBottomSheetDialog(periods)
    }

    private fun populateCalendar(calendar: Calendar){
        progressDialog.show()
        val periods = setUpPeriodsCalendar(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR))
        adapter.setMonthAndYearInCalendar(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR))
        ioThenMain({
            mainViewModel.populatePeriodsCalendar(periods, calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR))
        }, {
            populatePeriodList(it)
            progressDialog.hide()
        })
    }

    private fun populatePeriodList(mList: List<Periods>?){
        if (!mList.isNullOrEmpty()) {
            binding.calendar.rcyCalender.visibility = View.VISIBLE
            binding.calendar.tvNotAvailable.visibility = View.GONE
            adapter.addItems(mList)
        } else {
            binding.calendar.rcyCalender.visibility = View.GONE
            binding.calendar.tvNotAvailable.visibility = View.VISIBLE
        }
    }

    private fun showBottomSheetDialog(periods: Periods) {
        val bottomSheetDialog = BottomSheetDialog(this)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_period)
        val btnBleedingStart = bottomSheetDialog.findViewById<Button>(R.id.btn_bleeding)!!
        val btnBleedingEdit = bottomSheetDialog.findViewById<Button>(R.id.btn_edit_bleeding)!!
        val btnBleedingFinish = bottomSheetDialog.findViewById<Button>(R.id.btn_end_bleeding)!!
        val btnBleedingSpend = bottomSheetDialog.findViewById<Button>(R.id.btn_spend_bleeding)!!

        if(periods.isPeriod) btnBleedingEdit.text = "Remove Bleeding"
        else btnBleedingEdit.text = "Add Bleeding"

        btnBleedingStart.setOnClickListener {
            bottomSheetDialog.dismiss()
            val mCal = normalizeCalendar(Calendar.getInstance())
            val calendar = Calendar.getInstance()
            calendar.time = periods.gregorianDate
            val periodCal = normalizeCalendar(calendar)
            if(mCal.timeInMillis < periodCal.timeInMillis){
                toast("Your selected day is not same today")
            } else{
                startPeriodRoutine(periods)
            }
        }

        btnBleedingEdit.setOnClickListener {
            bottomSheetDialog.dismiss()
            val mCal = normalizeCalendar(Calendar.getInstance())
            val calendar = Calendar.getInstance()
            calendar.time = periods.gregorianDate
            val periodCal = normalizeCalendar(calendar)
            if(periods.isPeriod){
                if(mCal.timeInMillis < periodCal.timeInMillis){
                    toast("Your selected day is not same today")
                } else{
                    removePeriodRoutine(periods)
                }
            } else {
                 if(mCal.timeInMillis < periodCal.timeInMillis) {
                    toast("Your selected day is not same today")
                 }else{
                     addPeriodRoutine(periods)
                }
            }
        }

        btnBleedingFinish.setOnClickListener {
            bottomSheetDialog.dismiss()
            val mCal = normalizeCalendar(Calendar.getInstance())
            val calendar = Calendar.getInstance()
            calendar.time = periods.gregorianDate
            val periodCal = normalizeCalendar(calendar)
            if(!periods.isPeriod){
                toast("Please select periods day")
            } else if(mCal.timeInMillis < periodCal.timeInMillis) {
                toast("Your selected day is not same today")
            } else{
                endPeriodRoutine(periods)
            }
        }

        btnBleedingSpend.setOnClickListener {
            bottomSheetDialog.dismiss()
            if(periods.isPeriod){
               spendPeriodRoutine(periods)
            } else  toast("Please select periods day")
        }

        bottomSheetDialog.show()
    }

    private fun startPeriodRoutine(periods: Periods){
        ioThenMain({
            mainViewModel.startRoutine(periods)
        },{
            popupStartBleedingRule(periods, it!!)
        })
    }

    private fun addPeriodRoutine(periods: Periods){
        ioThenMain({ mainViewModel.isCurrentMonthRoutineStarted(periods)},{
            if(it!!){
                ioThenMain({
                    mainViewModel.submitAddRoutine(1, periods)
                },{
                    if(it!!) populateCalendar(mCal)
                    else toast("please add period after last day of current period.")
                })
            }else{
                toast("please starts periods days.")
            }
        })
    }

    private fun removePeriodRoutine(periods: Periods){
        ioThenMain({ mainViewModel.isCurrentMonthRoutineStarted(periods)},{
            if(it!!){
                ioThenMain({
                    mainViewModel.submitRemoveRoutine(periods)
                },{
                    if(it!!) populateCalendar(mCal)
                    else toast("please remove period after start day of current period.")
                })
            }else{
                toast("please starts periods days.")
            }
        })
    }

    private fun endPeriodRoutine(periods: Periods){
        ioThenMain({
            mainViewModel.isCurrentMonthRoutineStarted(periods)
        },{
            if(it!!){
                ioThenMain({
                    mainViewModel.endRoutine(periods)
                },{
                    val code = it!!
                    ioThenMain({
                        val law = mainViewModel.getMessageForBleedingRule(code)
                        mainViewModel.getLawMakingRule(law, code)
                    },{
                        popupEndBleedingRuleForRegular(periods, it!!, code)
                    })
                })
            }else{
                toast("please starts periods days.")
            }
        })
    }

    private fun spendPeriodRoutine(periods: Periods){
        val nCal = normalizeCalendar(Calendar.getInstance())
        val calendar = Calendar.getInstance()
        calendar.time = periods.gregorianDate
        val periodCal = normalizeCalendar(calendar)
        ioThenMain({
            mainViewModel.isCurrentMonthRoutineStarted(periods)
        },{
            if(it!!){
                if(nCal.timeInMillis < periodCal.timeInMillis) {
                    toast("Your selected day is not same today")
                }else{
                    ioThenMain({
                        mainViewModel.spendDailyRoutine(periods)
                    },{
                        populateCalendar(mCal)
                    })
                }
            }else toast("please starts periods days.")
        })
    }

    private fun popupStartBleedingRule(periods: Periods,code: Int){
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Start Bleeding Rule")
        builder.setMessage(mainViewModel.getMessageForBleedingRule(code))
        builder.setPositiveButton("Yes" ) { dialog, which ->
            dialog.dismiss()
            if(MessageType.CASE_INVALID.value != code){
                ioThenMain({
                    mainViewModel.bleedingProcessForPeriod(1, periods, code)
                },{
                    if(it!!) populateCalendar(mCal)
                    else toast("case failed!")
                    progressDialog.hide()
                })
            }
        }
        builder.setNegativeButton("No" ) { dialog, which -> dialog.cancel() }
        builder.setIcon(R.mipmap.ic_launcher)
        builder.show()
    }

    private fun popupCurrentBleedingStatus(message: String, code: Int){
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("End Bleeding Rule")
        builder.setMessage(message)
        builder.setNegativeButton("Ok" ) {
            dialog, which -> dialog.cancel()
        }
        builder.setIcon(R.mipmap.ic_launcher)
        builder.show()
    }

    private fun popupEndBleedingRuleForRegular(periods: Periods, message: String, code: Int){
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("End Bleeding Rule")
        builder.setMessage(message)
        builder.setPositiveButton("OK" ) { dialog, which ->
            dialog.cancel()
            if(code > 0){
                ioThenMain({
                    mainViewModel.submitFinishRoutine(1, periods, code, 0)
                },{
                    if(it!!) populateCalendar(mCal)
                    else toast("submission failed!")
                })
            }
        }
        builder.setIcon(R.mipmap.ic_launcher)
        builder.show()
    }

    private fun validationOfCurrentBleedingStatus(){
        ioThenMain({
          mainViewModel.validationOfCurrentStatus()
        },{
            if(it != null){
                val code = it
                if(code != MessageType.CASE_INVALID.value){
                    ioThenMain({
                        val law = mainViewModel.getMessageForBleedingRule(code)
                        mainViewModel.getLawMakingRule(law, code)
                    },{
                        if(!it.isNullOrEmpty()) popupCurrentBleedingStatus(it, code)
                    })
                }
            }
        })
    }
}