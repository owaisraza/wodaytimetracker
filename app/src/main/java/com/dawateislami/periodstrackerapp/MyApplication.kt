package com.dawateislami.periodstrackerapp

import android.app.Application
import com.dawateislami.periodstrackertool.components.WomenCalendar
import com.dawateislami.periodstrackertool.data.database.PeriodsDatabase
import com.dawateislami.periodstrackertool.manager.DataManager
import com.dawateislami.periodstrackertool.manager.LawsManager
import com.dawateislami.periodstrackertool.manager.OperationManager
import com.dawateislami.periodstrackertool.manager.PopulationManager
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MyApplication : Application() , KodeinAware {

    override val kodein = Kodein.lazy{
        import(androidXModule(this@MyApplication))

        //Database
        bind() from singleton { PeriodsDatabase.invoke(instance()) }


        //Managers
        bind() from singleton { WomenCalendar(instance()) }

        //viewModel
        bind() from singleton { MainViewModel(instance()) }

        //Providers
        bind() from provider { MainFactory(instance()) }

        // repository
    }
}