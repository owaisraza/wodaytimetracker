package com.dawateislami.periodstrackerapp

import com.dawateislami.periodstrackertool.data.Periods

interface CalendarCallback{
    fun onCalendar(periods: Periods)
}